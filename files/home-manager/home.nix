{ config, pkgs, lib, ... }:

let
  user = builtins.getEnv "USER";
  isLinux = pkgs.stdenv.isLinux;
  isDarwin = pkgs.stdenv.isDarwin;
  homeDirectory = if isLinux then
				  "/home/${user}"
				else if isDarwin then
				  "/Users/${user}"
				else
				  "/home/defaultuser";
  # linux, macOS共通のパッケージ
  commonPackages = with pkgs; [
	pkgs.hello
	pkgs.fortune
	pkgs.bat
	pkgs.btop
	pkgs.htop
	pkgs.zip
	pkgs.unzip
	pkgs.unrar
	pkgs.git
	pkgs.ripgrep
	pkgs.ncdu
	pkgs.whois
	pkgs.tree
	pkgs.tig
	pkgs.jq
	pkgs.zellij

	# python
	pkgs.pipx
	pkgs.pyenv
	# (pkgs.python3.withPackages (ps: with ps; [
    #   numpy
    #   requests
	#   pip
    #   # 他に必要なパッケージをここに追加
    # ]))

	# 開発
	pkgs.jdk11
	pkgs.rustup
	pkgs.cmake
	pkgs.gcc
	pkgs.go
	# ダウンローダー
	pkgs.yt-dlp
	pkgs.youtube-dl
	pkgs.aria2
	pkgs.wget
	pkgs.curl

	# クラウド開発
	pkgs.google-cloud-sdk
	pkgs.awscli
	pkgs.terraform

	# 仮想化
	pkgs.docker
	pkgs.docker-compose
	pkgs.podman # dockerの代替
	pkgs.trivy # コンテナの脆弱性スキャン

	# node
	# pkgs.nodejs

	# ネットワーク
	pkgs.tailscale
	pkgs.wireguard-tools

	# エディタ
	pkgs.vim

	# ターミナル
	pkgs.tmux

	# シェル
	pkgs.zsh
	pkgs.bash

	# コマンド
	pkgs.fd
	pkgs.fzf
	pkgs.peco
  ];
  # Linux特有のパッケージ
  linuxPackages = with pkgs; [
	pkgs.direnv # ディレクトリごとに環境変数を設定
    # pkgs.gnumake
	# pkgs.glibcLocales # 多言語対応
	# pkgs.glibcLocalesUtf8 # 多言語対応
	# pkgs.fast-cli # ネットワーク速度計測
	# pkgs.pyenv # pythonバージョン管理
  ];
  # macOS特有のパッケージ
  darwinPackages = with pkgs; [
	pkgs.wezterm
	pkgs.iterm2
	pkgs.vscode
	pkgs.raycast
	pkgs.karabiner-elements # キーバインド変更
	pkgs.goku # キーバインド変更
  ];
  # OSに応じて追加されるパッケージ
  osSpecificPackages = if isLinux then linuxPackages
                       else if isDarwin then darwinPackages
                       else [ ];
  finalPackages = commonPackages ++ osSpecificPackages;
in
{
  # 商用ソフトウェアをインストールするために必要
  nixpkgs = {
    config = {
      allowUnfree = true;
      allowUnfreePredicate = (_: true);
    };
  };
  # Home Manager needs a bit of information about you and the paths it should
  # manage.
  home.username = "${user}";
  home.homeDirectory = "${homeDirectory}";
  home.stateVersion = "23.11"; # Please read the comment before changing.

  # home.language = {
  #   base = "en_US.utf8";
  #   ctype = "en_US.utf8";
  #   numeric = "en_US.utf8";
  #   time = "en_US.utf8";
  #   collate = "en_US.utf8";
  #   monetary = "en_US.utf8";
  #   messages = "en_US.utf8";
  #   paper = "en_US.utf8";
  #   name = "en_US.utf8";
  #   address = "en_US.utf8";
  #   telephone = "en_US.utf8";
  #   measurement = "en_US.utf8";
  # };

  home.packages = finalPackages;

  # Home Manager is pretty good at managing dotfiles. The primary way to manage
  # plain files is through 'home.file'.
  home.file = {
	# tmux
	".tmux.conf".source = "${config.home.homeDirectory}/.var/dots/files/tmux/tmux.conf";

	# zellij
	".config/zellij/config.kdl".source = "${config.home.homeDirectory}/.var/dots/files/zellij/config.kdl";

	# git
	".gitconfig".source = "${config.home.homeDirectory}/.var/dots/files/git/gitconfig";

	# alacritty
	".alacritty.yml".source = "${config.home.homeDirectory}/.var/dots/files/alacritty/alacritty.yml";

	# dircolors
	".dircolors".source = "${config.home.homeDirectory}/.var/dots/files/dircolors";

	# ssh
	".ssh/config".source = "${config.home.homeDirectory}/.var/dots/files/ssh/config";

	# wezterm
	".config/wezterm/wezterm.lua".source = "${config.home.homeDirectory}/.var/dots/files/wezterm/wezterm.lua";

	# jupyter notebook, juptyer lab
	".jupyter/jupyter_notebook_config.py".source = "${config.home.homeDirectory}/.var/dots/files/jupyter/jupyter_notebook_config.py";
	".jupyter/jupyter_lab_config.py".source = "${config.home.homeDirectory}/.var/dots/files/jupyter/jupyter_lab_config.py";
  };

  # authorized_keysの配置とパーミッション設定
  home.activation.authorizedKeys = lib.hm.dag.entryAfter ["writeBoundary"] ''
    install -D -m600 ${config.home.homeDirectory}/.var/dots/files/ssh/authorized_keys $HOME/.ssh/authorized_keys
  '';

  # neovimの設定ファイルのシンボリックリンクを作成
  home.activation.createNeovimConfigLink = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
    rm -rf $HOME/.config/nvim
    ln -s ${config.home.homeDirectory}/.var/dots/files/nvim $HOME/.config/nvim
  '';
  
  # zshの設定ファイルのシンボリックリンクを作成
  home.activation.createZshConfigLink = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
    rm -rf $HOME/.zshrc
    ln -s ${config.home.homeDirectory}/.var/dots/files/zsh/zshrc-base $HOME/.zshrc
  '';

  # nix-envで`nixpkgs.glibcLocales`をインストールする
  home.activation.installGlibcLocales = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
  	# nix-env -iA nixpkgs.glibcLocales
  '';

  programs.git = {
    enable = true;
    userName  = "Gimpei Kobayashi";
    userEmail = "gimpeik@icloud.com";
	extraConfig = {
      core = {
        quotepath = "false";
        editor = "vim";
      };
      init = {
        defaultBranch = "main";
      };
    };
  };

  programs.bash = {
    enable = true;
    initExtra = ''
      # setlocale: LC_ALL: cannot change locale (en_US.UTF8)を回避するために設定
	  export BASH_SILENCE_DEPRECATION_WARNING=1
    '';
    profileExtra = ''
	  export BASH_SILENCE_DEPRECATION_WARNING=1
	  export LC_ALL=ja_JP.UTF-8
	  export LANG=ja_JP.UTF-8
	  export LANGUAGE=ja_JP.UTF-8
    '';
  };

  home.sessionVariables = {
    # EDITOR = "vim";
	# LC_ALL = "ja_JP.UTF-8";
	# TEST="this is text";
	# LOCALE_ARCHIVE_2_11 = "/usr/bin/locale/locale-archive";
	# LOCALE_ARCHIVE_2_27 = "${pkgs.glibcLocales}/lib/locale/locale-archive";
	# LOCALE_ARCHIVE = "${pkgs.glibcLocales}/lib/locale/locale-archive";
  };


  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;


  # systemd servicesを設定する例
  systemd.user.services.home-manager-service = {
    Unit = {
      Description = "systemd service ";
	  After = [ "network.target" ];
    };
    Install = {
      WantedBy = [ "default.target" ];
    };
    Service = {
	  Type = "simple";
      ExecStart = "${pkgs.writeShellScript "home-manager-service" ''
        #!/bin/sh
		echo 1
      ''}";
	  Restart = "always";
	  RestartSec = 5;
    };
  };
}
