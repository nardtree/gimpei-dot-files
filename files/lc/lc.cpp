
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <iostream>

int count(long int* cnt, int depth, const char* name) {
	struct dirent* entry;
	DIR* dir = opendir(name);
	if(dir == NULL) {
	  (*cnt)++;
	  return 1;
	}

	while((entry = readdir(dir)) != NULL) {
	  if(strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
		continue;
	  //printf("iter, %s/%s depth %d flag %d cnt %d\n", name, entry->d_name, depth, (entry->d_type == DT_DIR), *cnt);
	  if(entry->d_type == DT_DIR) {
		char path[2048];
		snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
		depth = count(cnt, depth, path);
		(*cnt)++;
	  } else {
		(*cnt)++;
	  }
	}
	closedir(dir);
	return depth;
}

int main(int argc, char *argv[]) {
	if(argc != 2) {
	  printf("err\n");
	  return 0;
	}
	long cnt = 1;
	int depth = count(&cnt, 1, argv[1]);
    printf("%ld\n",cnt);
    return 0;
}
