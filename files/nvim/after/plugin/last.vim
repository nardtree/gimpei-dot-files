

"""カスタムハイライティング"""
function CustomHighlight()
  " .vimのカスタム
  if @% =~# "\.vim$"

    syn region VimHereDoc start=/"""/ end=/$/
    hi def link VimHereDoc Substitute
    hi Substitute cterm=bold ctermfg=231 ctermbg=24 gui=bold guifg=#f8f8f2 guibg=#204a87

    syn match VimNote /\v"!\s.+$/
    hi VimNote cterm=underline ctermfg=None ctermbg=None gui=underline guifg=None guibg=None
    
    " syn clear vimLineComment
    syn clear vimLineComment
    syn match VimMemo /\v"\/\/\s.*$/
    hi VimMemo cterm=underline ctermfg=None ctermbg=None gui=None guifg=#009369 guibg=None
  endif
  
  if @% =~# "\.md$"
    "! vim-lspが起動していたら停止する(Lspを止めると全て止まってしまう)
    " :LspStopServer
    " syntax on(syntax
    " onするとfencedのコードブロックのハイライトが無効になってしまう)
    syn region MyMetaDoc start=/^---/ end=/---/
    hi MyMetaDoc cterm=italic ctermfg=45 ctermbg=None gui=italic guifg=#00d7ff guibg=None
    syn match MyHeaders /^#.+$/
    hi def link MyHeaders mkdHeading
    hi mkdHeading cterm=bold,underline ctermfg=None ctermbg=None gui=bold,underline guifg=None guibg=None
    hi htmlH1 cterm=bold,underline gui=bold,underline
    hi htmlH2 cterm=bold,underline gui=bold,underline
    hi htmlH3 cterm=bold,underline gui=bold,underline
    hi htmlH4 cterm=bold,underline gui=bold,underline
  endif
  
  "! .tmux.confのカスタム
  if @% =~# "\.tmux.conf$"
    syn match TmuxNote /\v##!\s.+$/
    hi TmuxNote cterm=underline ctermfg=None ctermbg=None gui=underline guifg=None guibg=None
  endif

  "! .pyのカスタム
  if @% =~# "\.py$"
    " treesitterをoff
    TSToggle highlight
  endif
endfunction
let timer_custom_highlight = timer_start(500, {-> execute("call CustomHighlight()", "")}, {'repeat': 1})

""" ノーマルモード基本操作関連 """
" "! fコマンドの拡張
" function FindChars()
"   let c0 = nr2char(getchar())
"   let c1 = nr2char(getchar())
"   let query = c0.c1
"   call feedkeys("/".query."\<CR>")
" endfunction
" nmap <buffer><silent> f :<C-u>call FindChars()<CR>
" "! fコマンドの拡張(二文字で検索できるようにする)
" function FindChars2()
"   let c0 = nr2char(getchar())
"   let c1 = nr2char(getchar())
"   let c2 = nr2char(getchar())
"   let query = c0.c1.c2
"   call feedkeys("/".query."\<CR>")
" endfunction
" nmap <buffer><silent> F :<C-u>call FindChars2()<CR>

lua <<EOF
-- 関数は`:call v:lua.wrap_gg()`で呼び出せる
function _G.wrap_gg()
  -- local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  vim.cmd([[call PutMark()]])
  vim.cmd([[call setpos(".", [0, 0, 0, 0])]])
end

vim.api.nvim_set_keymap(  'n', 
                          'gg',
                          ':call v:lua.wrap_gg()<CR>',
                          { noremap = true, silent = true })

function _G.wrap_G()
  -- local row, col = unpack(vim.api.nvim_win_get_cursor(0))
  vim.cmd([[call PutMark()]])
  vim.cmd([[call setpos(".", [0, line("$"), 0, 0])]])
end
vim.api.nvim_set_keymap(  'n', 
                          'G',
                          ':call v:lua.wrap_G()<CR>',
                          { noremap = true, silent = true })
EOF



"! -,=で次のparenthesに移動
function! GotoNextParenthes()
py3 << EOF
# 行数
height = int(vim.eval("line('$')"))
cwindow = vim.current.window
crow, ccol = cwindow.cursor
CHARS = {"\"", "{", "(", "[", "]", ")", "}"}
def main():
  for row, buffer in enumerate(vim.current.buffer, 1):
    if row < crow:
      continue
    col_idx = 0
    for buf_char in buffer:
      col_idx += 1
      if row == crow:
        if col_idx <= ccol+1:
          continue
      # 操作
      if buf_char in CHARS:
        print(f'''call setpos(".", [0, {row}, {col_idx}, 0])''')
        vim.command(f'''call setpos(".", [0, {row}, {col_idx}, 0])''')
        return
main()
EOF
endfunction
nmap <silent> = :call GotoNextParenthes()<CR>

function! GotoPrevParenthes()
py3 << EOF
# 行数
height = int(vim.eval("line('$')"))
cwindow = vim.current.window
crow, ccol = cwindow.cursor
CHARS = {"\"", "{", "(", "[", "]", ")", "}"}
def main():
  buffers = list(vim.current.buffer)
  for row in range(crow, 0, -1):
    # print(row, buffers)
    buffer = buffers[row-1]
    col_size = len(buffer)-1
    for col in range(col_size-1, 0, -1):
      buf_char = buffer[col]
      if row == crow:
        if col >= ccol:
          continue
      if buf_char in CHARS:
        # print(f'''call setpos(".", [0, {row}, {col}, 0])''')
        vim.command(f'''call setpos(".", [0, {row}, {col+1}, 0])''')
        return 
main()
EOF
endfunction
nmap <silent> - :call GotoPrevParenthes()<CR>

""" ノーマルモードのショートカットを設定 """
"! PageUp, PageDownがないハードウェアで同等のことを再現する
nmap <A-UP> <PageUp>
nmap <A-Down> <PageDown>
nmap <C-Up> <PageUp>
nmap <C-Down> <PageDown>

"! undoを無効化
" nmap u :<CR>
"! undoをCtrl+zに割当
nmap <C-z> :undo<CR>
"! redoをCtrl+rに割当(デフォルトと同じ)
nmap <C-r> :redo<CR>
"! ノーマルモードのwを:wに割当
nmap W :w<CR>
"! ノーマルモードのqを:qに割当
nmap Q :q<CR>
"! ハイライトを表示する
nmap HL :highlight<CR>
"! インクリメンタルサーチのハイライトを消す
nmap HR :nohl<CR>
"! マークされた文字へジャンプ
nmap <unique> < `[
nmap <unique> > `]
"! 自動マークオプション
nmap <unique><silent> M :call PutMark()<CR>
"! すべてのマークを削除
nmap <unique><silent> DM :call DelMark()<CR>
"! 今のマークを削除
nmap <unique><silent> U :call UnMark()<CR>
"! トレーニングモード
nmap <Up> <Nop>
nmap <Down> <Nop>
nmap <Left> <Nop>
nmap <Right> <Nop>
"! ALT(Option) + Arrowで前次の単語
nmap <A-Left> b
nmap <A-Right> w


""" インサートモード基本操作 """
imap <C-h> <Left>
imap <C-j> <Down>
imap <C-k> <Up>
imap <C-l> <Right>
": 匿名レジストリの貼り付け
imap <C-v> <C-r>"
"! ALT(Option) + Arrowで前次の単語
imap <A-Left> <C-\><C-O>b
imap <A-Right> <C-\><C-O>w


""" コマンドモード基本操作関連 """
"! Ctrl+aで先頭に移動
cmap <C-a> <HOME>
"! Ctrl+eで末尾に移動
cmap <C-e> <END>


""" ALE関連 """
" ALEのオンオフ
nmap A :ALEToggle<CR>
" ALEのFixerの適応
nmap <M-f> :ALEFix<CR>
nmap ƒ :ALEFix<CR> " <M-f>とƒは同一

""" vim-lsp関連 """
" vim-lspで、宣言された変数を移動する
" nmap <buffer> { <plug>(coc-references-used)
" nmap <buffer> } :echo "次の参照"<CR>:LspNextReference<CR>
" vim-lspで、定義された変数の名前を変更する
nmap <buffer> <M-r> <plug>(coc-rename)
"// vim-lspで、ライブラリや関数の詳細を得る
"// nmap <buffer> H <plug>(lsp-hover)
" vim-lspで、ライブラリや関数の定義を確認する
nmap <buffer> <M-d> <plug>(lsp-definition)

""" カラーテーマ関連 """
" 色の変更
" nmap <unique><silent> c :<C-u>call RndColo()<CR>:<C-u>call CustomHighlight()<CR>
" 背景の色を深い黒にする
nmap <unique><silent> B :hi Normal ctermbg=0 guibg=#000000<CR>:hi NonText ctermbg=0 guibg=#000000<CR>
" Pでいまカーソルがある場所のカラーグループを抽出
nmap P :echo synIDattr(synID(line("."), col("."), 1), "name")<CR>

" option + r => call registers(clipboard buffers)
nmap <Leader>r :registers

""" リーダーキー関連 """
" リーダーキーをスペースに設定
let mapleader = "\<Space>"
" split vertiacal
nmap <Leader>v :vs<CR>
" split horizontal
nmap <Leader>h :sp<CR>
nmap <Leader><Up> <C-w>k
nmap <Leader><Down> <C-w>j
nmap <Leader><Left> <C-w>h
nmap <Leader><Right> <C-w>l

vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P

" 小さいターミナルを立ち上げる
nmap <Leader>t :<C-u>call DeolFloating()<CR>
" 絵文字コードがある時、絵文字を適応
nmap <Leader>e :<C-u>call ApplyEmoji()<CR>
" タブをスペースに書き換える
nmap <C-s><C-t> :<C-u>call Tab2Space()<CR>

""" syntaxのon off """
nmap s :if exists("g:syntax_on") <Bar>
      \   syntax off <Bar>
      \ else <Bar>
      \   syntax enable <Bar>
      \ endif <CR>

""" treesitterのtoggle """
nmap t :<C-u>TSToggle highlight<CR>

