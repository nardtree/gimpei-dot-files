
" set shiftwidth=2
set expandtab       " タブを空白に変換
set tabstop=4       " タブキーを押したときに表示される空白の幅
set shiftwidth=4    " 自動インデントに使用される空白の幅
set softtabstop=4   " タブキーとバックスペースキーの動作を制御
