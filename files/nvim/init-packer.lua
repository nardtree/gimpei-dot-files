-- packerのbootstrapping
print('load init-packer.lua')

local install_path = vim.fn.stdpath('data')..'/site/pack/packer/start/packer.nvim'
if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  packer_bootstrap = vim.fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
  vim.cmd([[packadd packer.nvim]])
end

return require('packer').startup(function(use)
  -- packer自身
  use 'wbthomason/packer.nvim'
  -- vim-plug
  use 'junegunn/vim-plug'
  -- カラーテーマ生成ツール
  use 'rktjmp/lush.nvim'
  -- カラーテーマ
  use 'scysta/pink-panic.nvim'
  use 'olimorris/onedark.nvim'
  use 'casonadams/walh'
  use 'folke/tokyonight.nvim'
  use 'rebelot/kanagawa.nvim'
  use "arturgoms/moonbow.nvim"
  use { "catppuccin/nvim", as = "catppuccin" }
  -- use {'shaunsingh/oxocarbon.nvim', run = './install.sh'}
  -- treesitter
  -- use {'nvim-treesitter/nvim-treesitter', opt = true}
  -- noice.nvimのインストール
  use({
    "folke/noice.nvim",
    event = "VimEnter",
    config = function()
      require("noice").setup()
    end,
    requires = {
      -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
      "MunifTanjim/nui.nvim",
      -- OPTIONAL:
      --   `nvim-notify` is only needed, if you want to use the notification view.
      --   If not available, we use `mini` as the fallback
      "rcarriga/nvim-notify",
      }
  })
  -- codewindow(vs codeのようなコードのオーバービュー)
  use({
  'gorbit99/codewindow.nvim',
  config = function()
    local codewindow = require('codewindow')
    codewindow.setup()
    -- codewindow.apply_default_keybinds()
	-- active_in_terminals = true
	-- auto_enable = true
  end,
  })
  -- bufferへのファジーファインダー
  use 'ggandor/lightspeed.nvim'
  -- ペットをnvimに召喚するプラグイン
  use({
    "giusgad/pets.nvim",
    requires = {
	  "edluffy/hologram.nvim",
	  "MunifTanjim/nui.nvim",
    }
  })
  -- nvim-surround(カッコを挿入しやすくする)
  -- https://github.com/kylechui/nvim-surround
  use({
    "kylechui/nvim-surround",
    tag = "*", -- Use for stability; omit to use `main` branch for the latest features
    config = function()
        require("nvim-surround").setup({
            -- Configuration here, or leave empty to use defaults
        })
    end
  })
end)

