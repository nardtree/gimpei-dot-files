print('start to load init-treesitter.lua.')

require("nvim-treesitter.configs").setup({
  ensure_installed = { "python", "markdown", "lua", "rust" },
  sync_install = false, -- デフォルト
  auto_install = true, -- 自動インストール
  ignore_install = { "javascript" },
  highlight = {
    enable = true,              -- false will disable the whole extension
    disable = { "ruby" },  -- list of language that will be disabled
  },
  indent = {
    enable = true
  },
  -- 大きいファイルのときは無効化
  disable = function(lang, buf)
    local max_filesize = 5 * 100 * 1024 -- 500 KB
    local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    if ok and stats and stats.size > max_filesize then
      return true
    end
  end,
  -- additional_vim_regex_highlighting = false,
})

