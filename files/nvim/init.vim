" set number
set relativenumber
" set notermguicolors
set termguicolors
set mouse=r
set clipboard=unnamed
set tabstop=4
set shiftwidth=2
set encoding=utf-8
set termencoding=utf-8
set fenc=utf-8
set sh=zsh
set syntax=off
" 画面右端で折り返さない
set nowrap
" leaderキーの設定
map , <Leader>
nnoremap <leader>t :echo("\<leader\> works! It is set to '<leader>'")<CR>
nnoremap <Leader>a ggVG


""" cmdheight=0に設定
source ~/.config/nvim/init-cmdheight.vim


""" osのバージョン種類を取得 """
if !exists("g:os")
  if has("win64") || has("win32") || has("win16")
    let g:os = "Windows"
  else
    let g:os = substitute(system('uname'), '\n', '', '')
    " macos -> Darwin
    " linux -> Linux
  endif
endif


""" pythonのパスを動的に特定 """
let g:python3_host_prog = trim(system('which python3'))
""" python2は使用しないのでubuntuのデフォルトpathを割り当てる """
let g:python2_host_prog = expand('/usr/bin/python')

""" pythonの必要なモジュールがなかった場合、自動でインストール """
""" 2024-06-09 importlib.util.find_specが使えない """
function! GetLibs()
py3 << EOF
import importlib
import pip
import os
oks = []
os.environ["PIP_BREAK_SYSTEM_PACKAGES"] = "1"
for pkg_name in ["neovim", "black", "autopep8", "flake8", "pylint", "mypy", "yapf"]:
    if importlib.util.find_spec(pkg_name) is None:
        pip.main(["install", pkg_name])
    else:
        oks.append(pkg_name)
print(f"these packages are ok. {','.join(oks)}")
EOF
endfunction
" :call GetLibs()

""" mypyの設定があるかどうかチェック(無ければ設定を作成) """
function! CheckMypyini()
py3 << EOF
mypyini='''[mypy]
python_version = 3.9
# any型で返す関数の警告は表示しない
warn_return_any = False 
warn_unused_configs = True
ignore_missing_imports = True
'''
from pathlib import Path
# if not Path("~/.mypy.ini").expanduser().exists():
with Path("~/.mypy.ini").expanduser().open("w") as fp:
  fp.write(mypyini)
EOF
endfunction
:call CheckMypyini()


source ~/.config/nvim/init-random-colorshemes.vim

""" 自動マーク(アルファベットを指定しない) """
let g:put_mark_cur = 0
function! PutMark()
py3 << EOF
import string
import vim
lets = list(string.ascii_letters)
cur = int(vim.eval("g:put_mark_cur"))
while True:
  if vim.eval(f'getpos("\'{lets[cur]}")') == ["0", "0", "0", "0"]:
    vim.command(f'call setpos("\'{lets[cur]}", getpos("."))')
    vim.command("SignatureRefresh")
    break
  else:
    cur += 1
    cur %= len(lets)
cur += 1
cur %= len(lets)
vim.command(f"let g:put_mark_cur = {cur}")
vim.command("call HLMarks()")
EOF
endfunction

""" 今のマークを削除する """
function! UnMark()
  let idx = char2nr('a')
  while idx <= char2nr('z')
    if getpos(".") == getpos("'".nr2char(idx))
      exe 'delmarks' nr2char(idx)
    endif
    let idx = idx + 1
  endwhil
  :call HLMarks() " マークを再描画
  :SignatureRefresh " 左のシグネチャをリフレッシュ
  :wviminfo! " aaaマークの状態を保存 
endfunction 

let g:ascii_letters = []
let idx = char2nr('A')
while idx <= char2nr('z')
  if nr2char(idx) =~# "[a-zA-Z]"
    let g:ascii_letters = g:ascii_letters + [nr2char(idx)]
  endif
  let idx = idx + 1
endwhile

function! Test()
  let lines = line('$')
  let h = 1
  while h < lines
    let h += 1
    if getline(h) =~# "^#"
      echo getline(h)
    endif
  endwhile
endfunction

""" マークされている箇所をカスタムハイライト """
let g:hl_marks = [] 
function! HLMarks()
  " :call clearmatches()
  hi MARKS cterm=underline ctermbg=197 guibg=#875fff
  "! 前のマーク情報の削除
  for m in g:hl_marks
    :call matchdelete(m)
  endfor
  let g:hl_marks = [] " クリア
  let idx = char2nr('a')
  while idx < char2nr('z')
    "! call matchadd('MARKS', '\%'.line( "'".nr2char(idx)).'l' )
    "! matchaddpos("ErrorMsg", [[getpos("'b")[1], getpos("'b")[2], 1], 0])
    let m = matchaddpos("MARKS", [[getpos("'".nr2char(idx))[1], getpos("'".nr2char(idx))[2], 1], 0])
    let g:hl_marks = g:hl_marks + [m]
    let idx = idx + 1
  endwhile
endfunction
let timer_hl_marks = timer_start(100, {-> execute(":call HLMarks()", "")}, {'repeat': 1})

""" マークをすべて削除 """
function! DelMark()
    :delmarks a-zA-Z0-9
    :SignatureRefresh " 左のシグネチャをリフレッシュ
    :wviminfo! " マークの状態を保存
    :call HLMarks()
endfunction

""" foldingのフォーマットがイケてないので機能拡張 """
function! MyFoldText()
  let nblines = v:foldend - v:foldstart + 1
  let w = winwidth(0) - &foldcolumn - (&number ? 8 : 0)
  let line = getline(v:foldstart)
  let comment = substitute(line, '/\*\|\*/\|{{{\d\=', '', 'g')
  let expansionString = repeat(".", w - strwidth(nblines.comment.'"'))
  let txt = comment . "@folding@" . nblines
  return txt
endfunction
set foldtext=MyFoldText()
" set foldcolumn=1
"! foldmethodをmanualに(pluginが変な影響を及ぼす)
set foldmethod=manual
"! foldedのカラーフォーマットを編集
highlight Folded ctermbg=7 ctermfg=0
highlight FoldColumn ctermbg=black ctermfg=222 cterm=bold

""" 作成したfoldingを保存 """
augroup remember_folds
  try
    autocmd!
    autocmd BufWinLeave ?* silent! mkview
    autocmd BufWinEnter ?* silent! loadview
  catch
    echo "missed mkview"
  endtry
augroup END

""" termnalを起動 """
function Terminal()
  :terminal
  :startinsert
endfunction

""" Deolを起動 (小さいターミナル) """
function DeolFloating()
  :Deol -split=floating
endfunction


""" packerでのプラグイン管理 """
luafile ~/.config/nvim/init-packer.lua

""" パッケージマネージャplugの設定 """
source ~/.config/nvim/init-plug.vim

""" jediの設定 """
let g:jedi#goto_stubs_command = "<leader>c"
let g:jedi#goto_command = "<leader>g"
let g:jedi#use_splits_not_buffers = "left"
let g:jedi#popup_on_dot = 0
let g:jedi#show_call_signatures = 0 " 関数のシグネチャを描画する設定, osxで動作が不安定
let g:jedi#completions_enabled = 0 " デフォルトのjedi-vimを無効にしてdeoplete-jediを優先する -> vim-lsp, coc.nvimに優先する

""" vim-markdownの設定 """
source ~/.config/nvim/init-vim-markdown.vim

""" scrollbar.nvimの設定 """
augroup ScrollbarInit
  autocmd!
  autocmd WinScrolled,VimResized,QuitPre * silent! lua require('scrollbar').show()
  autocmd WinEnter,FocusGained           * silent! lua require('scrollbar').show()
  autocmd WinLeave,BufLeave,BufWinLeave,FocusLost            * silent! lua require('scrollbar').clear()
augroup end

""" 拡張子で挙動を変える設定 """
" py, cpp, md, vim, .zshrc, .bashrcならば以下の処理を実行
if @% =~# '\.py$' || @% =~# '\.cpp$' || @% =~# '\.md$' || @% =~# '\.vim$' || @% =~# 'shrc$' || @% =~# '\.nix$'
  set softtabstop=0 noexpandtab
  if @% =~# '\.vim$' || @% =~# 'shrc$' || @% =~# '\.md$'
    " vim, shrc, mdはタブはスペース２つ
    set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab
  else
    " py, cppはタブはスペース４つ
    set tabstop=4 softtabstop=0 expandtab shiftwidth=4 smarttab
  endif
  set expandtab 
  " \tコードの文字があった場合▷▷▷▷と表示する
  set listchars=tab:▷▷
  set nolist
  :echom "tab設定等を初期化"

  " pyは特殊なカラースキームを設定
  if @% =~# '\.py$'
    " :syntax off
    " colorscheme monokai-high
  " sqlのカラースキームを設定
  elseif @% =~# '\.sql$'
    colorscheme darkblue
  else
    :call RndColo()
  endif
endif

set shiftwidth=2

""" aleオプション """
let g:ale_completion_enabled = 1
let g:ale_completion_autoimport = 1
let g:ale_echo_msg_format='[%linter%][%severity%][%code%][%s]'
let b:ale_linters = ['flake8', 'mypy', 'pylint']
let b:ale_fixers = ['yapf']
" 一秒後に自動で:ALEToggleすることで一旦隠す
let timer_ale_toggle = timer_start(3000, {-> execute(":ALEToggle", "")}, {'repeat': 1})
" aleのflake8のオプション
let g:ale_python_flake8_options = '--max-line-length=128 --ignore=W605,C0114,E1136'
let g:ale_python_flake8_use_global = 1
" aleのpylintのオプション
let g:ale_python_pylint_use_global = 1
" nerdfontの文字を適応
let g:ale_sign_error = ''
let g:ale_sign_warning = ''

""" nvim-treesitter """
luafile ~/.config/nvim/init-treesitter.lua

""" nvim-colorizerの設定 """
lua <<EOF
require('colorizer').setup {
  'markdown';
  'vim';
  'css';
  'javascript';
  html = {
    mode = 'foreground';
  }
}
EOF


""" afinity(https://github.com/delphinus/artify.nvim) """
lua <<EOF
local artify = require('artify')
EOF

""" pets.nvim """
lua <<EOF
require("pets").setup({
  -- your options here
})
EOF

""" lualineの設定 """
luafile ~/.config/nvim/init-lualine.lua


""" nvim luaのユーザ設定 """
luafile ~/.config/nvim/init-example.lua


""" codewindowの設定 """
luafile ~/.config/nvim/init-codewindow.lua

""" noice.nvimの設定
luafile ~/.config/nvim/init-noice.lua


""" vim-lspの設定 """
" vim-lsp設定
"" 診断を無効化(ALEを使う)
let g:lsp_diagnostics_enabled = 0
let g:lsp_format_sync_timeout = 1000

let g:lsp_settings = {
\  'pylsp': {
\    'workspace_config': {
\      'pylsp': {
\        'enable': 0,
\        'configurationSources': ['pycodestyle', 'flake8'],
\        'plugins': {
\          'jedi_completion': {
\            'cache_for': ['pandas', 'numpy', 'tensorflow', 'matplotlib']
\          },
\          'jedi_hover': {
\            'enabled': 0
\          },
\          'jedi_signature_help': {
\            'enabled': 0
\          },
\          'flake8': {
\            'enabled': 1
\          },
\          'mccabe': {
\            'enabled': 0
\          },
\          'pycodestyle': {
\            'enabled': 0
\          },
\          'pyflakes': {
\            'enabled': 0
\          },
\          'pylsp_mypy': {
\            'enabled': 1
\          },
\        }
\      }
\    }
\  }
\}



""" coc.nvimの設定 """
luafile ~/.config/nvim/init-coc-nvim.lua

""" blackの設定 """
source ~/.config/nvim/init-black.vim

""" copilotの設定 """
luafile ~/.config/nvim/init-copilot.lua
