""" コマンドパレットの高さを設定する
""" neovim ver 0.8.0.0より上からコマンドパレットの高さを0に設定できるようになった

if v:version >= 800
  echom "setting up cmdheight=0"
  set cmdheight=0
endif
