
" blackの設定を変数の設定で行うことができる
" 具体的な設定項目については、プラグインのコードを参照
" https://github.com/psf/black/blob/main/plugin/black.vim

" PEP8は80文字が基本
let g:black_linelength = 80 + 21
echo "setted black options"
