require("plenary.reload").reload_module("lualine", true)

require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'auto',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff'}, --, 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {
	  -- lualineがクラッシュするため無効化
      -- {
      --     -- neovimのバージョンを表示
      --     function()
      --       local ret = {}
      --       for line in string.gmatch(vim.api.nvim_exec(':version', true), "%S+") do
      --         table.insert(ret, line)
      --       end
      --       return ret[2]
      --     end,
      --     icon = '',
      --     color  = {fg='#FFFFFF', bg='#000000'}
      -- },
	  {
          function()
            local ret = {}
            for line in string.gmatch(vim.api.nvim_exec(':LspStatus', true), "%S+") do
              table.insert(ret, line)
              -- break
            end
            return table.concat(ret, '')
          end,
          icon = '',
          color  = {fg='#FFFFFF', bg='#643B1B'}
      },
      {
        -- カラーテーマの表示
        function()
          return vim.g.colors_name
        end,
        icon = '', -- What ever icon you want
        color  = {fg='#171717', bg='#6A9AFF'}
      },
      {
         -- 行末の空白の検出
         function()
           return vim.fn.search([[\s\+$]], 'nw') ~= 0 and 'contains blanks' or ''
         end,
         icon = '', -- What ever icon you want
         color  = {bg='#FFFD8B'}
      },
	  {
        -- tabとspaceの混合indentの検出
        function()
          local space_indent = vim.fn.search([[\v^ +]], 'nw') > 0
          local tab_indent = vim.fn.search([[\v^\t+]], 'nw') > 0
          local mixed = (space_indent and tab_indent) or (vim.fn.search([[\v^(\t+ | +\t)]], 'nw') > 0)
          return mixed and 'mixed-indent' or ''
        end,
        icon = '',
        color = {bg='#bb2200'}
      }
    }
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
 },
  tabline = {},
  extensions = {}
}
