
""" colorschmeをランダムでセット """
function! RndColo()
py3 << EOF
import random
import vim
cannot_use_colos = ["solarized8"]
# colos = ["kanagawa", "monokai", "molokai", "icansee", "gruvbox", "1989", "Benokai", "0x7A69_dark", "iceberg", "pink-panic", "tokyonight", "tabula", "moonbow", "catppuccin-latte"]
colos = ["catppuccin"]
colo = random.choice(colos)
vim.command("colorscheme " + colo)
print(colo + " was selected.")
EOF
endfunction

nmap <unique><silent> c :<C-u>call RndColo()<CR>:<C-u>call CustomHighlight()<CR>
colorscheme catppuccin
