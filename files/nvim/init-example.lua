
function _G.test()
  print("test")
  return '<CR>'
end

vim.api.nvim_set_keymap(
  'n', -- ノーマルモードで
  'test', -- "test"を押すと
  -- ':echo "a"<CR>', -- コマンドを実行
  'v:lua.test()', -- luascriptの呼び出し
  {expr = true, noremap = true}
)


