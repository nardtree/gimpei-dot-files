
let g:vim_markdown_folding_disabled=1
" 見えなくなる文字がありみずらい
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0
let g:markdown_fenced_languages = ['coffee', 'css', 'erb=eruby', 'javascript', 'js=javascript', 'json=javascript', 'ruby', 'python', 'python3', 'sh', 'console=sh', 'sass', 'xml']
" /nvim/runtime/ftplugin/のshiftwidthへのオーバーライドを防ぐ
let g:markdown_recommended_style=0
