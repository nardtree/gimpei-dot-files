
""" パッケージマネージャpluggedの設定 """
call plug#begin('~/.config/nvim/plugged')
  "// 言語特有のプラグイン
  Plug 'udalov/kotlin-vim'
  Plug 'rust-lang/rust.vim'
  Plug 'nvie/vim-flake8'
  Plug 'tell-k/vim-autopep8'
  "// カラースキームごった煮
  Plug 'flazz/vim-colorschemes'
  "// 個別のカラースキーム
  Plug 'morhetz/gruvbox'
  Plug 'patstockwell/vim-monokai-tasty'
  Plug 'shaunsingh/oxocarbon.nvim', { 'do': './install.sh' }
  "// ファイル中のカラーコードをその色にハイライトする
  Plug 'norcalli/nvim-colorizer.lua'
  "// マークダウン拡張
  Plug 'kshenoy/vim-signature'
  Plug 'godlygeek/tabular'
  Plug 'plasticboy/vim-markdown'
  "// スクロールバー
  Plug 'Xuyuanp/scrollbar.nvim'
  "// treesitter
  Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
  "// ポップアップ
  "// :lua require('popup').create({"Hello", "World"}, {border=true, title='test', line="cursor+1", col="cursor+0", minwidth=25, minheight=15})
  Plug 'nvim-lua/popup.nvim'
  "// 非同期で様々な機能のフレー不ワークを提供するライブラリ
  Plug 'nvim-lua/plenary.nvim'
  "// telescope
  Plug 'nvim-telescope/telescope.nvim'
  Plug 'nvim-telescope/telescope-symbols.nvim'
  "// cmp-dictionary(辞書ベースの何でも補完)
  " Plug 'neovim/nvim-lspconfig'
  " Plug 'hrsh7th/cmp-nvim-lsp'
  " Plug 'hrsh7th/cmp-buffer'
  " Plug 'hrsh7th/cmp-path'
  " Plug 'hrsh7th/cmp-cmdline'
  " Plug 'hrsh7th/nvim-cmp'
  " Plug 'uga-rosa/cmp-dictionary'
  "// ale
  Plug 'dense-analysis/ale'
  "// jedi
  Plug 'davidhalter/jedi-vim'
  "// deoplete関連
  "// deoplete-jedi
  Plug 'deoplete-plugins/deoplete-jedi'
  "// Autopep8
  Plug 'tell-k/vim-autopep8'
  "// Black
  " Plug 'psf/black', { 'tag': '19.10b0' }
  Plug 'psf/black'
  "// vimairline関連
  " Plug 'vim-airline/vim-airline'
  " Plug 'vim-airline/vim-airline-themes'
  "// lualine.nvim
  Plug 'nvim-lualine/lualine.nvim'
  " Plug 'kyazdani42/nvim-web-devicons'
  "// affinity
  Plug 'delphinus/artify.nvim'
    "// vim-lsp関連
  "// vim-lspは機能の多くを上書き、無効化してしまうので導入時には気をつける
  Plug 'prabirshrestha/vim-lsp'
  "// Plug 'mattn/vim-lsp-settings'
  "// Plug 'prabirshrestha/asyncomplete.vim'
  "// Plug 'prabirshrestha/asyncomplete-lsp.vim'
  Plug 'Shougo/ddc.vim'
  Plug 'shun/ddc-vim-lsp'
  "// 導入テスト
  Plug 'bfredl/nvim-ipy'
  if g:os == "Linux"
    "// 導入テスト
    "// Plug 'neoclide/coc.nvim', {'branch': 'release'}
  endif
  " Plug 'neoclide/coc.nvim', {'branch': 'master'}
  Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}
  "Plug 'pappasam/coc-jedi', { 'do': 'yarn install --frozen-lockfile && yarn build', 'branch': 'main' }
call plug#end()
