
require("codewindow").setup {
  auto_enable = false -- 自動で有効化するかしないか
}

vim.api.nvim_set_keymap(
  'n', -- ノーマルモードで
  '<Leader>c', -- leader + c を押すと
  -- ':echo "a"<CR>', -- コマンドを実行
  ':lua require("codewindow").toggle_minimap()<CR>',
  {}
)
