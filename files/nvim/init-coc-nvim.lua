-- coc-nvimの設定

vim.g.coc_global_extensions = { 'coc-deno', 'coc-json', 'coc-yaml', 'coc-pyright', 'coc-rust-analyzer', 'coc-clangd' }
-- navigate diagnostic
vim.api.nvim_set_keymap('n', '[g', '<Plug>(coc-diagnostic-prev)', {noremap = false, silent = true})
vim.api.nvim_set_keymap('n', ']g', '<Plug>(coc-diagnostic-next)', {noremap = false, silent = true})


vim.api.nvim_set_keymap('n', 'gd', '<Plug>(coc-definition)', {noremap = false, silent = true})
vim.api.nvim_set_keymap('n', 'gy', '<Plug>(coc-type-definition)', {noremap = false, silent = true})
vim.api.nvim_set_keymap('n', 'gi', '<Plug>(coc-implementation)', {noremap = false, silent = true})
vim.api.nvim_set_keymap('n', 'gr', '<Plug>(coc-references)', {noremap = false, silent = true})

-- show reference
vim.api.nvim_set_keymap('n', 'H', 'lua _G.show_documentation()', {noremap = false, silent = true})
if vim.fn.has('macunix') == 1 then
  vim.g.coc_config_home = "~/.config/nvim/coc-config-macunix"
end
-- function _G.show_documentation()
--  vim.fn.CocActionAsync('doHover')
-- end

-- 候補が表示されたときに選択するキーを決定
-- デフォルトでは Ctrl + y
-- vimscript, inoremap <expr> å coc#pum#visible() ? coc#_select_confirm() : coc#refresh()
vim.api.nvim_set_keymap('i', 'å', 'coc#pum#visible() ? coc#_select_confirm() : coc#refresh()', {noremap = true, silent = false, expr = true})
