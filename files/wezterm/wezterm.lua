
local wezterm = require 'wezterm';

-- カラースキームのリストを定義
local color_schemes = {
  "Gruvbox Dark",
  "Dracula",
  "Tokyo Night Storm",
}

-- カラースキームをランダムに選ぶ関数
local function random_color_scheme(window)
  math.randomseed(os.time())
  local random_scheme = color_schemes[math.random(#color_schemes)]
  window:set_config_overrides({
    color_scheme = random_scheme,
  })
  wezterm.log_info("Changed color scheme to: " .. random_scheme)
  -- 画面上にカラースキーム名を表示
  window:toast_notification("Color Scheme Changed", "Current scheme: " .. random_scheme, nil, 4000)
end

-- 右側の空き領域
wezterm.on('update-right-status', function(window, pane)
  local date = wezterm.strftime '%Y-%m-%d %H:%M:%S'

  -- Make it italic and underlined
  window:set_right_status(wezterm.format {
    { Attribute = { Underline = 'Single' } },
    { Attribute = { Italic = true } },
    { Text = '😊' .. date },
  })
end)

local config = {
  disable_default_key_bindings = true, -- デフォルトのキーバインドを無効
  -- font = wezterm.font("PlemolJP Console NF"), -- フォントの設定
  -- 記述順にfont familyが適応される
  font = wezterm.font_with_fallback({
	  { family = "SFMono Nerd Font" },
	  { family = "PlemolJP Console NF" },
	}),
  window_decorations = "RESIZE", -- タイトルバーを非表示
  window_frame = {
    inactive_titlebar_bg = "none",
    active_titlebar_bg = "none",
  }, -- タブバー周りの表示を完結に
  show_new_tab_button_in_tab_bar = false, -- タブバーの「＋」を非表示
  show_close_tab_button_in_tabs = false, -- タブバーの「✗」を非表示
  tab_bar_at_bottom = true, -- タブバーをボトムに設定
  -- leaderの設定
  leader = { key = "q", mods = "CTRL", timeout_milliseconds = 2000 },
  keys = {
    -- コピーモードの設定
    {key="x", mods="LEADER", action=wezterm.action.ActivateCopyMode},
    {key="x", mods="CTRL|SHIFT", action=wezterm.action.ActivateCopyMode},
    {key="Escape", mods="NONE", action="ClearSelection"},
    -- コピペ
    {key="c", mods="SUPER", action=wezterm.action{CopyTo="Clipboard"}},
    {key="v", mods="SUPER", action=wezterm.action{PasteFrom="Clipboard"}},
    {key="t", mods="SUPER", action=wezterm.action{SpawnTab="CurrentPaneDomain"}},
    {key="w", mods="CMD", action=wezterm.action{CloseCurrentTab={confirm=true}}}, -- 現在のタブを閉じる、閉じるときに確認をする
    -- タブの移動
    {key="[", mods="CTRL", action=wezterm.action{ActivateTabRelative=-1}},
    {key="]", mods="CTRL", action=wezterm.action{ActivateTabRelative=1}},
    {key="{", mods="CTRL|SHIFT", action=wezterm.action{MoveTabRelative=-1}}, -- CTRL+SHIFT+[
    {key="}", mods="CTRL|SHIFT", action=wezterm.action{MoveTabRelative=1}}, -- CTRL+SHIFT+]
    {key="|", mods="CTRL|SHIFT", action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
    {key="_", mods="CTRL|SHIFT", action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
    {key="+", mods="CMD|SHIFT", action="IncreaseFontSize"},
    {key="_", mods="CMD|SHIFT", action="DecreaseFontSize"},
    -- リロード
    {key="r", mods="CMD", action=wezterm.action_callback(function(window, pane) 
                                   random_color_scheme(window) 
                                 end),
    },
  },
  color_schemes = {
    ["Monokai Vivid Mod"] = {
      foreground = "#f9f9f9",
      background = "#121212",
      cursor_bg = "#fb0007",
      cursor_border = "#fb0007",
      cursor_fg = "#000000",
      selection_bg = "#ffffff",
      selection_fg = "#000000",
      ansi = {"#121212","#fa2934","#98e123","#fff30a","#0443ff","#f800f8","#01b6ed","#ffffff"},
      brights = {"#838383","#f6669d","#b1e05f","#fff26d","#0443ff","#f200f6","#51ceff","#ffffff"},
    }
  },
  -- color_scheme = "Monokai Vivid Mod", -- カラースキームの設定
  color_scheme = "Tokyo Night",
  window_padding = {
    left = 1,
    right = 1,
    top = 0,
    bottom = 0,
  },
  launch_menu = {
    {
      label = "htop",
      set_environment_variables = { PATH = "/usr/local/bin" },
      args = {"htop"},
    },
    {
      label = "zsh",
      set_environment_variables = { PATH = "/bin:/usr/bin:/usr/sbin:/sbin:/usr/local/bin" },
      args = {"zsh"},
    },
    {
      label = "Bash",
      args = {"bash", "-l"},
    },
  },
}


-- windowsで使用するとき
if wezterm.target_triple == "x86_64-pc-windows-msvc" then
  -- デフォルトのシェルをpowershellへ
  config.default_prog = { "pwsh" }
  table.insert(config.keys, {key="c", mods="CTRL|SHIFT", action=wezterm.action{CopyTo="Clipboard"}})
  table.insert(config.keys, {key="v", mods="CTRL|SHIFT", action=wezterm.action{PasteFrom="Clipboard"}})
  table.insert(config.keys, {key="t", mods="CTRL|SHIFT", action=wezterm.action{SpawnTab="CurrentPaneDomain"}})
  -- 右クリックでペースト
  config.mouse_bindings = {
    {
      event = { Up = { streak = 1, button = 'Right' } },
      mods = 'NONE',
      action = wezterm.action.Paste,
    },
  },
  table.insert(config.launch_menu, { label = "PowerShell", args = {"powershell.exe", "-NoLogo"} })
end


return config
