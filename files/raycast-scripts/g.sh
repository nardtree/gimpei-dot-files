#! /bin/bash

# Required parameters:
# @raycast.author Gimpei Kobaayshi
# @authorURL http://gink03.github.io/
# @raycast.schemaVersion 1
# @raycast.title g
# @raycast.mode silent
# @raycast.packageName Navigation

# Optional parameters:
# @raycast.icon images/dash.png
# @raycast.argument1 { "type": "text", "placeholder": "Query", "percentEncoded": true}

open -a Safari "https://www.google.com/search?q=$1"
