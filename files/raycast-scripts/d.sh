#! /bin/bash

# Required parameters:
# @raycast.author Gimpei Kobaayshi
# @authorURL http://gink03.github.io/
# @raycast.schemaVersion 1
# @raycast.title d
# @raycast.mode silent
# @raycast.packageName Navigation

# Optional parameters:
# @raycast.icon images/dash.png
# @raycast.argument1 { "type": "text", "placeholder": "検索語句", "percentEncoded": true}

open -a "Microsoft Edge" "https://duckduckgo.com/?q=$1"
