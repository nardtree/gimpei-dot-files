import os
from cryptography.fernet import Fernet
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.backends import default_backend
import base64
from typing import Union
import sys

def padding_key(key: bytes) -> bytes:
    kdf = PBKDF2HMAC(
        algorithm=hashes.SHA256(),
        length=32,
        salt=b'666',
        iterations=100000,
        backend=default_backend()
    )
    key = base64.urlsafe_b64encode(kdf.derive(key))
    return key


def encription(key: Union[str, bytes], text: [str, bytes]) -> bytes:
    if isinstance(key, str):
        key = bytes(key, 'utf8')
    key = padding_key(key)
    if isinstance(text, str):
        text = bytes(text, 'utf8')
    return Fernet(key).encrypt(text)


def decription(key: Union[str, bytes], ciphertext: bytes) -> str:
    if isinstance(key, str):
        key = bytes(key, 'utf8')
    key = padding_key(key)
    text = Fernet(key).decrypt(ciphertext)
    return text


if __name__ == "__main__":
    # this is a test case
    if '--encrypt' in sys.argv:
        text = input('please input target text:')
        key = input('please input target key:')
        ciphertext = encription(key, text) 
        print(ciphertext)

    else:    
        key = '123'
        ciphertext = encription(key=key, text='nya--n')
        print(decription(key=key, ciphertext=ciphertext))
        

