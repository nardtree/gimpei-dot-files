
import glob
from pathlib import Path
import zipfile
import shutil
from os import environ as E
import os
import datetime
import socket
from subprocess import Popen, PIPE

HOME = E.get("HOME")
HOSTNAME = socket.gethostname()

now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
shutil.make_archive(f'{HOME}/.tmp/{HOSTNAME}_{now}', 'zip', f'{HOME}/.config')

filename = f'{HOSTNAME}_{now}.zip'

# print(f'dbxcli put {HOME}/.tmp/{filename} /Backups/{filename}')

with Popen([f'dbxcli', 'put', f'{HOME}/.tmp/{filename}', f'/Backups/{filename}'], stdin=None, stdout=None, stderr=None) as proc:
    proc.communicate()

Path(f'{HOME}/.tmp/{filename}').unlink()
