import os
import sys

HOME = os.environ["HOME"]
os.system("curl -s https://get.sdkman.io | bash")
os.system(
    f"""/bin/bash -c "source {HOME}/.sdkman/bin/sdkman-init.sh && \
sdk install kotlin"
"""
)

os.system("sudo apt install openjdk-8-jdk")

# oracle JDK
os.system("sudo add-apt-repository ppa:linuxuprising/java && sudo apt update")
os.system("sudo apt install oracle-java13-set-default")

# switch to oracle jdk13
os.system("sudo apt install oracle-java13-set-default")
