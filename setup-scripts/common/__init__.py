
from pathlib import Path
import os
import importlib.util
import pip
from loguru import logger

def initializ():
    """
    必須モジュールがインストールされていなかったら、
    別途インストールする
    """
    for pkg_name in ["requests", "bs4", "loguru"]:
        if importlib.util.find_spec(pkg_name) is None:
            pip.main(["install", pkg_name])
initializ()

"""
gitのrootディレクトリを取得
"""
ROOT = Path(__file__).absolute().parent.parent.parent.__str__()
logger.info(f"ROOT = {ROOT}")

"""
ユーザのホームディレクトリを取得
"""
if os.environ.get("HOME"):
    HOME = os.environ["HOME"]
else:
    HOME = Path.home().__str__()
logger.info(f"HOME = {HOME}")
