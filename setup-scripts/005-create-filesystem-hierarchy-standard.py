#!/usr/bin/env python3
import os
from pathlib import Path

HOME = os.environ["HOME"]

# ~/.binを作成
Path(f"{HOME}/.bin").mkdir(exist_ok=True, parents=True)

# ~/.tmpを作成
Path(f"{HOME}/.tmp").mkdir(exist_ok=True, parents=True)

# ~/.varを作成
Path(f"{HOME}/.var").mkdir(exist_ok=True, parents=True)

# ~/.optを作成
Path(f"{HOME}/.opt").mkdir(exist_ok=True, parents=True)

# ~/.mntを作成
Path(f"{HOME}/.mnt").mkdir(exist_ok=True, parents=True)

# ~/.projectsを作成
Path(f"{HOME}/.projects").mkdir(exist_ok=True, parents=True)
