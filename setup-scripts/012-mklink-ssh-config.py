#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT


path_config = Path("~/.ssh/config").expanduser().absolute()
path_git_config = Path(ROOT) / "files/ssh/config"

if path_config.exists() and not path_config.is_symlink():
    logger.info("~/.zshrc is a file. try to unlink.")
    path_config.unlink()

if path_config.is_symlink():
    logger.info("~/.zshrc is symlink. try to unlink.")
    path_config.unlink()

path_config.symlink_to(path_git_config)
logger.info(f"create ~/.zshrc symlink to {path_git_config}")
