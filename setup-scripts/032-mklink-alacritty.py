#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT

path_conf = Path("~/.alacritty.yml").expanduser().absolute()
path_git_conf = Path(ROOT) / "files/alacritty.yml"

if path_conf.is_symlink():
    logger.info("~/.alacritty.yml is symlink. try to unlink.")
    path_conf.unlink()
elif path_conf.is_file():
    logger.info("~/.alacritty.yml is file. try to unlink.")
    path_conf.unlink()

path_conf.symlink_to(path_git_conf)
logger.info(f"create ~/.alacritty.yml symlink to {path_git_conf}")
