#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT

path_tmuxconf = Path("~/.tmux.conf").expanduser().absolute()
path_git_tmuxconf = Path(ROOT) / "files/tmux/tmux.conf"

if path_tmuxconf.is_symlink():
    logger.info("~/.tmuxconf is symlink. try to unlink.")
    path_tmuxconf.unlink()

path_tmuxconf.symlink_to(path_git_tmuxconf)
logger.info(f"create ~/.tmux symlink to {path_git_tmuxconf}")
