#!/bin/zsh

if [[ ! -d "tmux" ]]; then
  git clone https://github.com/tmux/tmux tmp_tmux
fi

if [[ `uname` = "Darwin" ]]; then
  for pkg in automake autoconf pkg-config utf8proc
  do
	brew install $pkg
  done
elif [[ `uname` = "Linux" ]]; then
  sudo apt install ncurses-dev libevent-dev autoconf automake pkg-config libbison-dev 
fi

cd tmp_tmux

git pull

make clean

sh ./autogen.sh

if [[ `uname` = "Darwin" ]]; then
  ./configure --disable-utf8proc
else
  ./configure
fi
make
sudo make install
