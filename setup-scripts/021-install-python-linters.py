from pathlib import Path
from common import logger, ROOT


def main():
    # flake8のセットアップ
    path_sym_flake8 = Path("~/.flake8").expanduser()
    path_real_flake8 = Path(ROOT + "/files/flake8")
    if path_sym_flake8.exists():
        path_sym_flake8.unlink(missing_ok=True)
        logger.info("~/.flake8をアンリンクしました")
    if path_sym_flake8.is_symlink():
        path_sym_flake8.unlink(missing_ok=True)
        logger.info("~/.flake8をアンリンクしました")

    path_sym_flake8.symlink_to(path_real_flake8)
    logger.info("~/.flake8のsymlinkを作成しました")
    
    # pylintrcのセットアップ
    path_sym_pylint = Path("~/.pylintrc").expanduser()
    path_real_pylint = Path(ROOT + "/files/pylintrc")
    if path_sym_pylint.exists():
        path_sym_pylint.unlink(missing_ok=True)
        logger.info("~/.pylintrcをアンリンクしました")
    if path_sym_pylint.is_symlink():
        path_sym_pylint.unlink(missing_ok=True)
        logger.info("~/.pylintrcをアンリンクしました")

    path_sym_pylint.symlink_to(path_real_pylint)
    logger.info("~/.pylintrcのsymlinkを作成しました")


if __name__ == "__main__":
    main()
