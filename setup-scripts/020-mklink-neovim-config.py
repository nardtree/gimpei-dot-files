import os
from pathlib import Path
import shutil
from common import logger, ROOT, HOME

def main():
    # filesのfile/nvimをsymlinkを作成する
    # - 1. unlink ~/.config/nvim
    # - 2. create ln -s ~/.config/nvim files/nvim
    Path(f"{HOME}/.config").mkdir(exist_ok=True)
    logger.info("~/.configを作成しました")

    if Path(f"{HOME}/.config/nvim").is_symlink():
        Path(f"{HOME}/.config/nvim").unlink(missing_ok=True)
    elif Path(f"{HOME}/.config/nvim").is_dir():
        shutil.rmtree(f"{HOME}/.config/nvim")
    logger.info("~/.config/nvimをアンリンクしました")

    # 手元のfile/nvimを~/.config/nvimにsymlinkを張る
    Path(f"{HOME}/.config/nvim").symlink_to(f"{ROOT}/files/nvim")

    # reqirementsを入れる
    os.system("python3 -m pip install neovim --upgrade")

if __name__ == "__main__":
    main()
