#!/usr/bin/env python3
import os
import shutil
from pathlib import Path
import glob
from loguru import logger

HOME = Path.home()
DIR = Path(__file__).absolute().parent.parent
path_bin = Path("~/.bin").expanduser()
path_git_bin = DIR / "bin"

if path_bin.is_symlink():
    path_bin.unlink()
path_bin.mkdir(exist_ok=True)

for orig_bin in path_git_bin.glob("*"):
    bin_name = orig_bin.name 
    tgt_bin = (path_bin / bin_name).absolute()
    if tgt_bin.exists() and tgt_bin.is_file():
        tgt_bin.unlink()
    if tgt_bin.is_dir():
        continue
    tgt_bin.symlink_to(orig_bin)
    logger.info(f"create {tgt_bin} symlink to {orig_bin}")
