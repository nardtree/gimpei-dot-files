#!/bin/zsh

if [[ ! -d "tmp_neovim" ]]; then
  git clone https://github.com/neovim/neovim/ tmp_neovim
fi


if [[ `uname` = "Darwin" ]]; then
  for pkg in automake autoconf pkg-config utf8proc
  do
	brew install $pkg
  done
elif [[ `uname` = "Linux" ]]; then
  for pkg in automake autoconf libtool cmake pkg-config gettext libtool-bin g++ unzip
  do
  	sudo apt install $pkg -y
  done
fi

cd tmp_neovim

git pull
make clean
make distclean

# CMAKE_BUILD_TYPE=RelWithDebInfoでログファイルの生成を抑止する
make -j 4 CMAKE_BUILD_TYPE=RelWithDebInfo 
sudo make install CMAKE_BUILD_TYPE=RelWithDebInfo
