#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT

Path("~/.config/zellij").expanduser().mkdir(exist_ok=True)

path_conf = Path("~/.config/zellij/config.kdl").expanduser().absolute()
path_git_conf = Path(ROOT) / "files/zellij-config.kdl"

if path_conf.is_symlink():
    logger.info("~/.config/zellij/config.kdl is symlink. try to unlink.")
    path_conf.unlink()
elif path_conf.is_file():
    logger.info("~/.config/zellij/config.kdl is file. try to unlink.")
    path_conf.unlink()

path_conf.symlink_to(path_git_conf)
logger.info(f"create ~/.config/zellij/config.kdl symlink to {path_git_conf}")
