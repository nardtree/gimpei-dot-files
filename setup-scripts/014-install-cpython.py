#!/usr/bin/env python3
import os
import sys
import requests
from bs4 import BeautifulSoup
from pathlib import Path
import re
import shutil
from concurrent.futures import ThreadPoolExecutor
from typing import List

import distro
import lxml
from loguru import logger

HOME = os.environ["HOME"]

distro_name = distro.name()
if distro_name in {"Ubuntu", "Debian GNU/Linux"}:
    # コンパイルを追加
    dependencies = """make build-essential libssl-dev zlib1g-dev libbz2-dev \
                    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
                    xz-utils tk-dev libffi-dev liblzma-dev git"""
    os.system(f"sudo apt install {dependencies} -y")
elif distro_name == "Darwin":
    os.system("brew install xz gdbm zlib")


def search_latest_python_url() -> List[str]:
    """github releaseのページから最新のpython releaseのURLを
    取得する
    """
    download_url = "https://github.com/python/cpython/tags"
    hrefs = []
    with requests.get(download_url) as r:
        soup = BeautifulSoup(r.text, "lxml")
    for e in soup.find_all("a", {"href": True}):
        href = e.get("href")
        if re.search(r"^/python/cpython/archive/refs/tags/v3.\d{1,}.\d{1,}.zip$", href):
            hrefs.append(href)
        # ページ送りする
        if e.text.strip() == "Next":
            download_url = e.get("href")
    for href in hrefs:
        logger.info(f"候補 {href}")
    return hrefs


def build_and_install(href):
    """ビルドとインストール
    """
    name = href.split("/")[-1]
    if not Path(f"{HOME}/.tmp/{name}").exists():
        os.system(f"wget https://github.com/{href} -O {HOME}/.tmp/{name}")
    ver = re.search("(3.\d{1,}.\d{1,})", name).group(1)
    if not Path(f"{HOME}/.tmp/cpython-{ver}").exists():
        os.system(f"unzip {HOME}/.tmp/{name} -d {HOME}/.tmp/ >/dev/null")
    if not Path(f"{HOME}/.opt/cpython-{ver}").exists():
        if distro_name in {"Ubuntu", "Debian GNU/Linux"}:
            configure_command = f"./configure --prefix {HOME}/.opt/cpython-{ver} >/dev/null"
        elif distro_name == "Darwin":
            configure_command = f""" CPPFLAGS="-I$(brew --prefix)/include -I$(brew --prefix zlib)/include" \
                LDFLAGS="-L$(brew --prefix)/lib -L$(brew --prefix zlib)/lib" \
                ./configure --with-pydebug \
                --with-openssl=$(brew --prefix openssl) \
                --prefix {HOME}/.opt/cpython-{ver} \
            """
        else:
            raise Exception(f"サポートされているOSに合致しません, {distro_name}")
        os.system(
            f"cd {HOME}/.tmp/cpython-{ver} && {configure_command} && make -j 16 >/dev/null 2>&1 &&  make install >/dev/null"
        )
    else:
        logger.info(f"すでに、cpython-{ver}はインストールされています,再インストールの際は、手動で削除してください")

def main():
    hrefs = search_latest_python_url()
    with ThreadPoolExecutor(max_workers=4) as exe:
        list(exe.map(build_and_install, hrefs))

if __name__ == "__main__":
    main()
