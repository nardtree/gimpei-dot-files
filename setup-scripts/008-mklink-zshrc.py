#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT


path_zshrc = Path("~/.zshrc").expanduser().absolute()
path_git_zshrc = Path(ROOT) / "files/zsh/zshrc-base"

if path_zshrc.exists() and not path_zshrc.is_symlink():
    logger.info("~/.zshrc is a file. try to unlink.")
    path_zshrc.unlink()

if path_zshrc.is_symlink():
    logger.info("~/.zshrc is symlink. try to unlink.")
    path_zshrc.unlink()

path_zshrc.symlink_to(path_git_zshrc)
logger.info(f"create ~/.zshrc symlink to {path_git_zshrc}")
