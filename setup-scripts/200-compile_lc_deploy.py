import os
import shutil
from pathlib import Path

if shutil.which("g++") is not None:
    compiler = shutil.which("g++")
elif shutil.which("clang++") is not None:
    compiler = shutil.which("clang++")
else:
    raise Exception("利用可能なC++コンパイラが指定されていません")

os.system(f"{compiler} -Wall -pedantic -Wunused-variable -o ~/.bin/lc files/lc/lc.cpp")
