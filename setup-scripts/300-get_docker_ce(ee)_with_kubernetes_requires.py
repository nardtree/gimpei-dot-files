from concurrent.futures import ProcessPoolExecutor
from pathlib import Path
import os
import sys
import pandas as pd
import requests
import distro
from bs4 import BeautifulSoup
import re
OSName, Version, CodeName = distro.linux_distribution(full_distribution_name=True)
print(f'Your OS is {OSName} {Version} {CodeName}')
# eg. https://download.docker.com/linux/ubuntu/dists/disco/pool/stable/amd64/
# poolの中にstable or nightlyのbuildがあって、それを取り出す

with requests.get(f"https://download.docker.com/linux/ubuntu/dists/") as r:
    if r.status_code != 200:
        raise Exception("cannot access to https://download.docker.com/linux/ubuntu/dists/")

    html = r.text
soup = BeautifulSoup(html, 'lxml')
distroName_url = {}
for a in soup.find_all('a', {'href': True}):
    distro_name = a.get('href').replace('/', '')
    if '..' in distro_name:
        continue
    URL = f'https://download.docker.com/linux/ubuntu/dists/{distro_name}/pool/stable/amd64/'
    with requests.get(URL) as r:
        if r.status_code != 200:
            continue
        sub = BeautifulSoup(r.text, 'lxml')
        DOWNLOAD_URLs = []
        for a in sub.find_all('a', {'href': re.compile('.deb$')}):
            if '..' in a.get('href'):
                continue
            download_url = URL + a.get('href')
            DOWNLOAD_URLs.append(download_url)
    distroName_url[distro_name] = DOWNLOAD_URLs
    print('target distro', distro_name, 'links', DOWNLOAD_URLs)

# print(distroName_url)
args = []
for distroName, urls in distroName_url.items():
    for url in urls:
        arg = (distroName, url)
        args.append(arg)


def download(arg):
    distroName, url = arg
    Path(f'downloads/{distroName}').mkdir(exist_ok=True, parents=True)
    if Path(f'downloads/{distroName}/{name}').exists():
        return
    name = url.split('/')[-1]
    try:
        with requests.get(url, timeout=20) as r:
            binary = r.content
        with open(f'downloads/{distroName}/{name}', 'wb') as fp:
            fp.write(binary)
    except Exception as exc:
        print(exc)


#for arg in args:
#    download(arg)

with ProcessPoolExecutor(max_workers=len(args)) as exe:
    exe.map(download, args)
