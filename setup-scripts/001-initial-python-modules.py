#!/usr/bin/env python3
import importlib.util
import pip

for pkg_name in [
    "tqdm",
    "bs4",
    "loguru",
    "lxml",
    "requests",
    "colored",
    "inquirer",
    "distro",
]:
    if importlib.util.find_spec(pkg_name) is None:
        pip.main(["install", pkg_name])


