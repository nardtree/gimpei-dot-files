import io
import tarfile
import requests
from bs4 import BeautifulSoup
import re
import os
import shutil

try:
    os.system('bash -c "pip install distro"')
    import distro
except Exception as exc:
    print(exc)
    exit(1)

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)

if distro_name.lower() in {"elementary", "ubuntu", "debian"}:
    matchs = re.compile("-linux-x86_64.tar.gz")
elif distro_name.lower() in {"darwin"}:
    matchs = re.compile("-darwin-x86_64.tar.gz")
else:
    print("サポート外のOSです")
    exit(1)

r = requests.get("https://cloud.google.com/sdk/docs/downloads-versioned-archives")
soup = BeautifulSoup(r.text, "html5lib")
for a in soup.find_all("a", {"href": matchs}):
    url = a.get("href")

r = requests.get(url)
with open("/tmp/cloud_sdk.tar.gz", "wb") as fp:
    fp.write(r.content)

HOME = os.environ["HOME"]
fp = tarfile.open("/tmp/cloud_sdk.tar.gz", "r:gz")
fp.extractall(f"{HOME}/.var/cloud_sdk")
if shutil.which("zsh") is not None:
    os.system(f"zsh {HOME}/.var/cloud_sdk/google-cloud-sdk/install.sh")
elif shutil.which("bash") is not None:
    os.system(f"bash {HOME}/.var/cloud_sdk/google-cloud-sdk/install.sh")
else:
    print("適切なshellが見つかりませんでした")
