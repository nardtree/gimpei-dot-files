import os
import platform
from subprocess import Popen
from subprocess import PIPE
import shutil
from loguru import logger
import distro

SOFTWARE_LIST = """
vim
tmux
clang
gcc
mosh
sshfs
httpie
pv
wget
curl
zsh
bat
progress
nfs-kernel-server
hddtemp
build-essential
curl
file
git
docker.io
docker-compose
rclone
cargo
aria2
golang
"""

distro_name, version, codename = distro.linux_distribution(full_distribution_name=False)
if distro_name.lower() in {"ubuntu", "debian", "elementary"}:
    software_list = [s.strip() for s in SOFTWARE_LIST.split("\n") if s.strip() != ""]
    for software in software_list:
        logger.info(f"sudo apt-get install {software}")
        with Popen(
            ["sudo", "apt-get", "install", software, "-y"],
            stdout=PIPE,
            stderr=PIPE,
            shell=False,
        ) as proc:
            stdout, stderr = proc.communicate()
            logger.info(stdout.decode("utf8", "ignore").replace("\n", " "))

    # docker groupに入ってなかったら追加する
    if "docker" not in os.popen("groups $USER").read():
        logger.info("try to put docker group to $USER")
        os.system("sudo usermod -aG docker $USER")
    logger.info("activate docker group.")
    os.system("newgrp docker")
elif distro_name.lower() in {"darwin"}:
    if shutil.which("brew") is not None:
        os.system("brew upgrade")
    else:
        os.system(
            'ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null'
        )

    software_list = """
    vim
    tmux
    clang
    gcc
    mosh
    sshfs
    httpie
    pv
    wget
    curl
    zsh
    bat
    progress
    exa
    aria2
    zzz
    rclone
    """
    # 先頭の空白を取り除く
    software_list = [s.strip() for s in software_list.split("\n") if s.strip() != ""]

    for software in software_list:
        logger.info(f"brew install {software}")
        with Popen(
            ["brew", "install", software],
            # stdout=PIPE,
            # stderr=PIPE,
            shell=False,
        ) as proc:
            stdout, stderr = proc.communicate()

else:
    logger.warning("サポート外のOSです")
