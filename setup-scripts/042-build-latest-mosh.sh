#!/bin/zsh

if [[ ! -d "tmp_mosh" ]]; then
  git clone https://github.com/mobile-shell/mosh.git tmp_mosh
fi


if [[ `uname` = "Darwin" ]]; then
  for pkg in automake autoconf pkg-config utf8proc
  do
	brew install $pkg
  done
elif [[ `uname` = "Linux" ]]; then
  sudo apt install libevent-dev autoconf automake pkg-config libbison-dev protobuf-compiler libssl-dev
fi

cd tmp_mosh

git pull
make clean
sh ./autogen.sh

if [[ `uname` = "Darwin" ]]; then
  ./configure --disable-utf8proc
else
  ./configure
fi

make
sudo make install
