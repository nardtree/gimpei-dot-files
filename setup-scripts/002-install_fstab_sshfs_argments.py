
"""
以下のrawsのテキストに書き足していくことで一般化する
"""
raws = """
gimpei@192.168.40.14:/home/gimpei /home/gimpei/.mnt/14 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.16:/home/gimpei /home/gimpei/.mnt/16 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.20:/home/gimpei /home/gimpei/.mnt/20 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.21:/home/gimpei /home/gimpei/.mnt/21 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0

gimpei@192.168.40.16:/home/gimpei/sdb /home/gimpei/.mnt/favs00 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.20:/home/gimpei/nvme0n1 /home/gimpei/.mnt/favs01 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.12:/home/gimpei/sda /home/gimpei/.mnt/favs02 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.12:/home/gimpei/sdd /home/gimpei/.mnt/favs03 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
gimpei@192.168.40.12:/home/gimpei/sdc /home/gimpei/.mnt/favs04 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0

# followers
gimpei@192.168.40.16:/home/gimpei/sdc /home/gimpei/.mnt/followers00 fuse.sshfs StrictHostKeyChecking=no,allow_other,reconnect,identityfile=/home/gimpei/.ssh/id_github 0 0
"""
import re
from pathlib import Path
from os import environ as E
raws = raws.split('\n')
fstab = open("/etc/fstab").read()
for raw in raws:
    params = re.split(r"\s{1,}", raw)
    """
    意味のあるフォーマットなら、6個からなるはず
    """
    if len(params) != 6:
        continue
    """
    すでにfstabに設定が書き込まれていたらスキップ
    """
    if params[0] in fstab:
        continue
    print("追加", raw)
    """
    そうでないなら設定をfstabのテキストに追加
    """
    fstab += raw + "\n"
    """
    ファイルパスが存在しないとマウントできないので作成する
    """
    Path(params[1]).mkdir(exist_ok=True, parents=True)

HOME = E.get("HOME")
with open(f"{HOME}/fstab", "w") as fp:
    fp.write(fstab)

