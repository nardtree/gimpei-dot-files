import os


# ubuntu linuxにzfsをインストールする
os.system("sudo apt-get install zfsutils-linux")

# zpool status
os.system("zpool status")

# spaning(ストライピングするだけ)
os.system("zpool create -f -m {mount point} pool_name devices[] ")

# raidz(raid5?)
os.system("zpool create -f -m {mount point} pool_name raidz devices[] ")

os.system("zpool list")

os.system("zpool destroy {pool_name}")

# limit arc(メモリを使いすぎるのを抑制する)
os.system("echo 'options zfs zfs_arc_max=870912' > /etc/modprobe.d/zfs.conf")

# 圧縮オプション
os.system('zfs set compression=lz4 <pool>')

# drop cache(メモリが暴走したときこれで復旧できる)
os.system('sync; echo 2 | sudo tee /proc/sys/vm/drop_caches')

# pagecacheが増えることもあり、こちらは3番
os.system('echo 3 | sudo tee /proc/sys/vm/drop_caches')

# primary cacheはmetadata限定推奨
## http://www.patrikdufresne.com/en/blog/2018-05-23-proxmox-primarycache-all-metadata/

# Linuxデフォルトのpage cacheがヤバいので確認できる
# 無効にしたいとき以下のパラメータを以下のファイルに追加
## sudo vi /etc/hdparm.conf
## write_cache = off
os.system("sudo hdparm -i /dev/sda")



