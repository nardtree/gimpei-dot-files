#!/bin/zsh

SCRIPT_PATH="$(dirname "$(realpath "$0")")"
PARENT_PATH="$(dirname "$SCRIPT_PATH")"

ln -s "$PARENT_PATH/files/jupyter/jupyter_notebook_config.py" $HOME/.jupyter/jupyter_lab_config.py
ln -s "$PARENT_PATH/files/jupyter/jupyter_notebook_config.py" $HOME/.jupyter/jupyter_notebook_config.py
