#!/usr/bin/env python3
from pathlib import Path
from common import logger, ROOT

path_dircolors = Path("~/.dircolors").expanduser().absolute()
path_git_dircolors = Path(ROOT) / "files/dircolors"

if path_dircolors.is_symlink():
    logger.info("~/.dircolors is symlink. try to unlink.")
    path_dircolors.unlink()

path_dircolors.symlink_to(path_git_dircolors)
logger.info(f"create ~/.dircolors symlink to {path_git_dircolors}")
