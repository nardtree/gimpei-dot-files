# GPUを再起動するスクリプト
# 管理者権限での実行が必要
# ref. https://www.wikihow.com/Reset-Graphics-Driver#:~:text=Press%20%E2%8A%9E%20Win%20+%20Ctrl%20+%20%E2%87%A7,graphics%20drivers%20have%20been%20reset.

import os
import re
from typing import Optional

def get_gpuid() -> Optional[str]:
    r = os.popen("pnputil /enum-devices /class Display").read()
    for line in r.split("\n"):
        if m := re.search(r"Instance ID:\s{1,}(.*?)$", line):
            return m.group(1).strip()
    return None

def main():
    if gpuid := get_gpuid():
        os.system(f'pnputil /restart-device "{gpuid}"')

if __name__ == "__main__":
    main()
