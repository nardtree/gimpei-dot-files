#!/bin/bash

# ホスト名で小文字で表示
HOSTNAME=`hostname | tr '[:upper:]' '[:lower:]'`

case $HOSTNAME in
  "kichijouji")
	tmux set -g status-bg "#FFB200"
	;;
  "fubuki")
	tmux set -g status-bg colour012
	;;
  "ogikubo")
	tmux set -g status-bg "#00FFB6"
	;;
  "eifukucho")
	tmux set -g status-bg "#F8E9FC"
	;;
  "kaga")
	# 192.168.*.21
	tmux set -g status-bg "#334efb"
	;;
  "kouenji")
	# 192.168.*.24
	tmux set -g status-bg "#f7dc6f"
	;;
  "akabane")
	# oci free
	tmux set -g status-bg "#fc441d"
	;;
  "kameari")
	# oci free
	tmux set -g status-bg "#00EEEE"
	;;
  "macminim1")
	tmux set -g status-bg "#229eff"
	;;
  o-*-mac)
	tmux set -g status-bg "#C8D8FF"
	;;
  *-mac)
	tmux set -g status-bg "#9CD5D6"
	;;
  *)
	# デフォルトケース
	echo "default colour uesed,".$HOSTNAME
	tmux set -g status-bg colour002
	;;
esac

