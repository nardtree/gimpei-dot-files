TMUX_CURRENT_WINDOW=`tmux display-message -p '#I'`
TMUX_CURRENT_SESSION=`tmux display-message -p '#S'`

if [ -f "$HOME/.tmp/tmux/${TMUX_CURRENT_SESSION}_${TMUX_CURRENT_WINDOW}" ]; then
  cat "$HOME/.tmp/tmux/${TMUX_CURRENT_SESSION}_${TMUX_CURRENT_WINDOW}"
else
  echo "ぽよ ~/.tmp/tmux/${TMUX_CURRENT_SESSION}_${TMUX_CURRENT_WINDOW}"
fi
