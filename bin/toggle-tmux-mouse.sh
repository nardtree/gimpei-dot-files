
now_status=`tmux show-options -g mouse`
# echo $now_status
if [ "$now_status" = "mouse on" ]; then
  toggle=off
else
  toggle=on
fi
# echo $toggle
tmux set-option -w mouse $toggle
tmux set-option -g mouse $toggle
tmux display-message "mouse is now: $toggle"
