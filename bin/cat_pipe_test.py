from subprocess import Popen
from subprocess import PIPE

with Popen(["cat"], stdin=PIPE, stdout=PIPE) as proc:

    with proc.stdin as fp:
        for ch in ["hello world!"]:
            fp.write(bytes(ch, "utf8"))
            fp.flush()

    with proc.stdout as fp:
        while True:
            ch = fp.read(1)
            if not ch:
                break
            print(ch.decode("utf8"), end="")
        print()
