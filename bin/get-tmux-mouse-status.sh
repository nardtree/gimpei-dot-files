
now_status=`tmux show-options -g mouse`
if [ "$now_status" = "mouse on" ]; then
  ret="#[fg=#000000,bold,bg=#82E0AA]ON#[default]" 
else
  ret="#[fg=white,bold,bg=colour197]OFF#[default]"
fi
echo $ret
