import datetime

now = datetime.datetime.now()
start = datetime.datetime(year=now.year, month=1, day=1)
end = datetime.datetime(year=start.year + 1, month=1, day=1)
a = (now - start).total_seconds() / (end - start).total_seconds()
print(f"今年、{a*100:0.04f}%%経過", end="")
