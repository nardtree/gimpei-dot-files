#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
# Matrix-Curses
# See how deep the rabbit hole goes.
# Copyright (c) 2012 Tom Wallroth
#
# Sources on github:
#   http://github.com/devsnd/matrix-curses/
#
# licensed under GNU GPL version 3 (or later)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#

from __future__ import unicode_literals
import locale
import time
import curses
import random
from collections import namedtuple
import sys

from pathlib import Path
import pandas as pd
import numpy as np

char_df = pd.read_csv(Path(__file__).parent.absolute() / "char.csv").head(500)
CHARS = char_df["char"].tolist()
WEIGHTS = char_df["freq"].astype(float).values
WEIGHTS /= WEIGHTS.sum()

PYTHON2 = sys.version_info.major < 3
locale.setlocale(locale.LC_ALL, '')
encoding = locale.getpreferredencoding()

########################################################################
# TUNABLES

DROPPING_CHARS = 150
MIN_SPEED = 1
MAX_SPEED = 2
RANDOM_CLEANUP = 100
WINDOW_CHANCE = 50
WINDOW_SIZE = 25
WINDOW_ANIMATION_SPEED = 3
FPS = 25
SLEEP_MILLIS = 1.0 / FPS
USE_COLORS = True
SCREENSAVER_MODE = True
MATRIX_CODE_CHARS = CHARS

########################################################################
# CODE

COLOR_CHAR_NORMAL = 1
COLOR_CHAR_HIGHLIGHT = 2
COLOR_WINDOW = 3


class FallingChar(object):
    matrixchr = list(MATRIX_CODE_CHARS)
    normal_attr = curses.A_NORMAL
    highlight_attr = curses.A_REVERSE

    def __init__(self, width, MIN_SPEED, MAX_SPEED):
        self.x = 0
        self.y = 0
        self.speed = 1
        self.char = '  '
        self.reset(width, MIN_SPEED, MAX_SPEED)

    def reset(self, width, MIN_SPEED, MAX_SPEED):
        # ランダムに文字を選択
        self.char = random.choices(population=FallingChar.matrixchr,
                                   weights=WEIGHTS,
                                   k=1)[0].encode(encoding)
        # 横は2の倍数
        self.x = random.choice([i for i in range(0, width - 1) if i % 2 != 1])
        self.y = 0
        self.speed = randint(MIN_SPEED, MAX_SPEED)
        # offset makes sure that chars with same speed don't move all in same frame
        self.offset = randint(0, self.speed * 2)

    def tick(self, scr, steps):
        height, width = scr.getmaxyx()
        if self.advances(steps):
            # if window was resized and char is out of bounds, reset
            # delete old char
            del_y = self.y - 14
            if del_y >= 0 and del_y <= height - 2:
                scr.addstr(del_y, self.x, '  ')

            self.out_of_bounds_reset(width, height)
            if self.y < height - 2:
                # make previous char curses.A_NORMAL
                if USE_COLORS:
                    scr.addstr(self.y, self.x, self.char,
                               curses.color_pair(COLOR_CHAR_NORMAL))
                else:
                    scr.addstr(self.y, self.x, self.char, curses.A_NORMAL)

                # choose new char and draw it A_REVERSE if not out of bounds
                self.char = random.choices(population=FallingChar.matrixchr,
                                           weights=WEIGHTS,
                                           k=1)[0].encode(encoding)
                self.y += 1
                if not self.out_of_bounds_reset(width, height):
                    if USE_COLORS:
                        scr.addstr(self.y, self.x, self.char,
                                   curses.color_pair(COLOR_CHAR_HIGHLIGHT))
                    else:
                        scr.addstr(self.y, self.x, self.char, curses.A_REVERSE)
            else:
                if self.y == height - 2:
                    if USE_COLORS:
                        scr.addstr(self.y, self.x, self.char,
                                   curses.color_pair(COLOR_CHAR_NORMAL))
                    else:
                        scr.addstr(self.y, self.x, self.char, curses.A_NORMAL)
                self.y += 1

    def out_of_bounds_reset(self, width, height):
        if self.x > width - 2:
            self.reset(width, MIN_SPEED, MAX_SPEED)
            return True
        if self.y > height + 13:
            self.reset(width, MIN_SPEED, MAX_SPEED)
            return True
        return False

    def advances(self, steps):
        if steps % (self.speed + self.offset) == 0:
            return True
        return False

    def step(self, steps, scr):

        return -1, -1, None


# we don't need a good PRNG, just something that looks a bit random.
def rand():
    # ~ 2 x as fast as random.randint
    a = 9328475634
    while True:
        a ^= (a << 21) & 0xffffffffffffffff
        a ^= (a >> 35)
        a ^= (a << 4) & 0xffffffffffffffff
        yield a


r = rand()


def randint(_min, _max):
    if PYTHON2:
        n = r.next()
    else:
        n = r.__next__()
    return (n % (_max - _min)) + _min


def main():
    steps = 0
    scr = curses.initscr()
    scr.nodelay(1)
    curses.curs_set(0)
    curses.noecho()

    if USE_COLORS:
        curses.start_color()
        curses.use_default_colors()
        curses.init_pair(COLOR_CHAR_NORMAL, curses.COLOR_BLUE,
                         curses.COLOR_BLACK)
        curses.init_pair(COLOR_CHAR_HIGHLIGHT, curses.COLOR_WHITE,
                         curses.COLOR_BLACK)
        curses.init_pair(COLOR_WINDOW, curses.COLOR_GREEN, curses.COLOR_GREEN)

    height, width = scr.getmaxyx()
    window_animation = None
    lines = []
    for i in range(DROPPING_CHARS):
        l = FallingChar(width, MIN_SPEED, MAX_SPEED)
        l.y = randint(0, height - 2)
        lines.append(l)

    scr.refresh()
    while True:
        height, width = scr.getmaxyx()
        for line in lines:
            line.tick(scr, steps)
        # for i in range(RANDOM_CLEANUP):
        #    x = randint(0, width-1)
        #    y = randint(0, height-1)
        #    scr.addstr(y, x, '  ')

        scr.refresh()
        time.sleep(SLEEP_MILLIS)
        if SCREENSAVER_MODE:
            key_pressed = scr.getch() != -1
            if key_pressed:
                raise KeyboardInterrupt()
        steps += 1


try:
    main()
except KeyboardInterrupt:
    curses.endwin()
    curses.curs_set(1)
    curses.reset_shell_mode()
    curses.echo()
