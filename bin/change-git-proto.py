#!/usr/bin/env python3
import re
import os

orig = os.popen("git config --get remote.origin.url").read().strip()
print(f'orig = {orig}')
if re.search("^https://.*?@bitbucket.org/", orig):
    tail = re.search("bitbucket.org/([^/]{1,}/[^/]{1,})$", orig).group(1)
    head = "git@bitbucket.org:"
    print("git remote set-url origin " + head + tail)
elif re.search("^https://gist.github.com", orig):
    tail = re.search("github.com/[^/]{1,}/([^/]{1,})$", orig).group(1)
    head = "git@gist.github.com"
    print("git remote set-url origin " + head + "/" +  tail)
elif re.search("^git@bitbucket.org", orig):
    tail = re.search(":(.*?)$", orig).group(1)
    head = "https://git@bitbucket.org/"
    print("git remote set-url origin " + head + tail)
