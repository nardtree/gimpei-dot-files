#!/usr/bin/env python3

# 一覧性が高いls

import curses
import glob
import collections
import string
import os
import re
import sys
from hashlib import sha512
import pickle
from pathlib import Path
import time
import pandas as pd
from loguru import logger

logger.remove()  # デフォルの設定をクリア

logger.add(
    os.path.join(os.environ.get("HOME"), ".config/perspective-ls/logs.log"))

Path(os.path.join(os.environ.get("HOME"),
                  ".config/perspective-ls/states")).mkdir(exist_ok=True)

ItemAsset = collections.namedtuple("ItemAsset",
                                   ["h", "w", "text", "item_name"])

W_WIDTH = 25
MAX_COLUMNS, MAX_LINES = os.get_terminal_size(0)
MAX_COL_SIZE = MAX_COLUMNS // W_WIDTH - 1
logger.info(f"MAX_COL_SIZE = {MAX_COL_SIZE}")

# ターミナルの場所分、２つ引く
MAX_LINES -= 1
# print(MAX_COLUMNS, MAX_LINES)

# プリント可能なascii文字
printable = set(string.printable)

filenames = []
for dirname, _, names in os.walk("."):
    # '.git'ディレクトリは対象としない
    if ".git" in dirname:
        continue
    for name in names:
        filenames.append(os.path.join(dirname, name))
FILENAME_DF = pd.DataFrame({"filename": sorted(filenames)})
FILENAME_DF["id"] = list(range(len(filenames)))
FILENAME_DF["page_num"] = [
    id // (MAX_LINES * MAX_COL_SIZE) for id in FILENAME_DF["id"]
]


def get_text_from_item(item: str):
    cnt = 0
    tmp = ""
    if item.count("/") > 0:
        target_item = item.split("/")[-1]
    else:
        target_item = item

    for char in target_item:
        if char in printable:
            cnt += 1
            tmp += char
        else:
            cnt += 2
            tmp += char
        if cnt >= W_WIDTH - 2:
            tmp += "*"
            break
    return tmp


def get_mat(page_num=0):

    colnum_items = collections.defaultdict(list)
    for i, filename in enumerate(
            FILENAME_DF.query("page_num == @page_num")["filename"]):
        colnum_items[i // MAX_LINES].append(filename)

    mat = [[" " for cn in range(MAX_COLUMNS + 1000)]
           for ln in range(MAX_LINES)]
    hw_item = {}
    for colnum, items in colnum_items.items():
        start_w = colnum * W_WIDTH
        for h_, item in enumerate(items):
            w_ = 0
            for char in get_text_from_item(item):
                if char in printable:
                    mat[h_][start_w + w_] = char
                    hw_item[(h_, start_w + w_)] = (h_, colnum)
                    w_ += 1
                else:
                    mat[h_][start_w + w_] = char
                    mat[h_][start_w + w_ + 1] = ""
                    hw_item[(h_, start_w + w_)] = (h_, colnum)
                    hw_item[(h_, start_w + w_ + 1)] = (h_, colnum)
                    w_ += 2
    return (mat, hw_item)


def get_item_matrix(page_num=0):
    colnum_items = collections.defaultdict(list)
    for i, filename in enumerate(
            FILENAME_DF.query("page_num == @page_num")["filename"]):
        colnum_items[i // MAX_LINES].append(filename)
    # ポジションから存在するitemを割り当て
    item_matrix = [[None for ln in range(MAX_LINES)] for cn in range(100)]
    for colnum, items in colnum_items.items():
        for h_, item in enumerate(items):
            start_w = colnum * W_WIDTH
            text = get_text_from_item(item)
            item_matrix[h_][colnum] = ItemAsset(h_, start_w, text, item)
    return item_matrix


screen = curses.initscr()
curses.curs_set(0)
screen.keypad(1)
curses.mousemask(1)
curses.noecho()


def draw_basic():
    for h, x in enumerate(MAT):
        for w, char in enumerate(x[:MAX_COLUMNS]):
            if h == MAX_LINES - 1 and w == MAX_COLUMNS - 1:
                continue
            try:
                screen.addstr(h, w, char)
                # logger.info(f"{char} h={h} w={w}, MAX_COLUMNS={MAX_COLUMNS}, MAX_LINES={MAX_LINES}")
            except Exception as exc:
                logger.warning(
                    f"{exc} h={h} w={w}, MAX_COLUMNS={MAX_COLUMNS}, MAX_LINES={MAX_LINES}"
                )


def highlighting(imh, imd):
    if ITEM_MATRIX[imh][imd] is None:
        return

    for ih, iw in [
        (min(imh + 1, MAX_LINES), imd),
        (max(imh - 1, 0), imd),
        (imh, min(imd + 1, MAX_COLUMNS)),
        (imh, max(imd - 1, 0)),
    ]:
        if ITEM_MATRIX[ih][iw] is None:
            continue
        h, w, text, _ = ITEM_MATRIX[ih][iw]
        screen.addstr(h, w, text)
    h, w, text, item = ITEM_MATRIX[imh][imd]
    screen.addstr(h, w, text, curses.A_REVERSE)
    # 下部にフルファイル名を表示
    screen.addstr(MAX_LINES, 0, " " * (MAX_COLUMNS - 1))
    screen.addstr(MAX_LINES, 0, item, curses.A_STANDOUT)


class State:

    def __init__(self):
        self.imh = 0
        self.imd = 0
        self.page_num = 0


STATE = State()
MAT, HW_ITEM, ITEM_MATRIX = None, None, None


def load_state():
    global STATE, MAT, HW_ITEM, ITEM_MATRIX
    digest = sha512(bytes(os.getcwd(), "utf8")).hexdigest()[:16]
    save_path = os.path.join(os.environ.get("HOME"),
                             f".config/perspective-ls/states/{digest}")
    if not Path(save_path).exists():
        PAGE_NUM = 0
        ITEM_MATRIX = get_item_matrix(page_num=PAGE_NUM)
        MAT, HW_ITEM = get_mat(page_num=PAGE_NUM)
        draw_basic()
        highlighting(STATE.imh, STATE.imd)
        return
    with open(save_path, "rb") as fp:
        STATE = pickle.load(fp)
    ITEM_MATRIX = get_item_matrix(page_num=STATE.page_num)
    MAT, HW_ITEM = get_mat(page_num=STATE.page_num)
    draw_basic()
    highlighting(STATE.imh, STATE.imd)


def save_state():
    digest = sha512(bytes(os.getcwd(), "utf8")).hexdigest()[:16]
    save_path = os.path.join(os.environ.get("HOME"),
                             f".config/perspective-ls/states/{digest}")
    with open(save_path, "wb") as fp:
        pickle.dump(STATE, fp)


def check_in_range(imh, imd, toward="UP|DOWN|RIGHT|LEFT"):
    if toward == "UP":
        imh -= 1
    elif toward == "RIGHT":
        imd += 1
    elif toward == "LEFT":
        imd -= 1
    elif toward == "DOWN":
        imh += 1
    else:
        raise Exception(f"illigal toward = {toward}")
    return ITEM_MATRIX[imh][imd]


# draw_basic()
load_state()
while True:
    event = screen.getch()
    if event == ord("q"):
        curses.endwin()
        save_state()
        sys.exit(0)
    if event == curses.KEY_ENTER or event == 10 or event == 13:
        curses.endwin()
        print(ITEM_MATRIX[STATE.imh][STATE.imd].item_name)
        save_state()
        sys.exit(0)
    if event in {ord("l"), ord("b"), ord("n")}:
        curses.endwin()
        item_name = ITEM_MATRIX[STATE.imh][STATE.imd].item_name
        if event in {ord("l"), ord("b")}:
            os.system(f"bat --paging=always --style=plain {item_name}")
        elif event == ord("n"):
            os.system(f"nvim {item_name}")
        else:
            raise Exception("Unknown command.")
        screen = curses.initscr()
        draw_basic()
        highlighting(STATE.imh, STATE.imd)
        screen.refresh()
        # sys.exit(0)
    if event == curses.KEY_MOUSE:
        id, mx, my, _, bstate = curses.getmouse()
        if HW_ITEM.get((my, mx)) is None:
            continue
        # ダブルクリックがハンドルできないので同じimh, imdだったら終了
        if (STATE.imh, STATE.imd) == HW_ITEM.get((my, mx)):
            save_state()
            curses.endwin()
            print(ITEM_MATRIX[STATE.imh][STATE.imd].item_name)
            sys.exit(0)
        # ダブルクリックでないときは通常描画
        STATE.imh, STATE.imd = HW_ITEM.get((my, mx))
        draw_basic()  # ジャンプするので描画し直し
        highlighting(STATE.imh, STATE.imd)
        screen.refresh()
        logger.info(f"{id}, {mx}, {my}, {bstate}")
    if event == curses.KEY_UP:
        if check_in_range(STATE.imh, STATE.imd, "UP"):
            STATE.imh -= 1
            STATE.imh = max(STATE.imh, 0)
            logger.info(ITEM_MATRIX[STATE.imh][STATE.imd])
            highlighting(STATE.imh, STATE.imd)
    if event == curses.KEY_DOWN:
        if check_in_range(STATE.imh, STATE.imd, "DOWN"):
            STATE.imh += 1
            STATE.imh = min(STATE.imh, MAX_LINES)
            logger.info(ITEM_MATRIX[STATE.imh][STATE.imd])
            highlighting(STATE.imh, STATE.imd)
    if event == curses.KEY_RIGHT:
        if check_in_range(STATE.imh, STATE.imd, "RIGHT"):
            STATE.imd += 1
            STATE.imd = min(STATE.imd, MAX_COLUMNS)
            logger.info(ITEM_MATRIX[STATE.imh][STATE.imd])
            highlighting(STATE.imh, STATE.imd)
    if event == curses.KEY_LEFT:
        if check_in_range(STATE.imh, STATE.imd, "LEFT"):
            STATE.imd -= 1
            STATE.imd = max(STATE.imd, 0)
            logger.info(ITEM_MATRIX[STATE.imh][STATE.imd])
            highlighting(STATE.imh, STATE.imd)
    # PAGE_DOWN == KEY_NPAGE
    if event == curses.KEY_NPAGE:
        logger.info("press KEY_NPAGE")
        STATE.page_num += 1
        if len(FILENAME_DF.query("page_num == @STATE.page_num")) == 0:
            STATE.page_num -= 1
            continue
        MAT, HW_ITEM = get_mat(page_num=STATE.page_num)
        ITEM_MATRIX = get_item_matrix(page_num=STATE.page_num)
        draw_basic()
        if ITEM_MATRIX[STATE.imh][STATE.imd] is None:
            STATE.imh = 0
            STATE.imd = 0
        highlighting(STATE.imh, STATE.imd)
    if event == curses.KEY_PPAGE:
        logger.info("press KEY_PPAGE")
        STATE.page_num -= 1
        if len(FILENAME_DF.query("page_num == @STATE.page_num")) == 0:
            STATE.page_num += 1
            continue
        MAT, HW_ITEM = get_mat(page_num=STATE.page_num)
        ITEM_MATRIX = get_item_matrix(page_num=STATE.page_num)
        draw_basic()
        if ITEM_MATRIX[STATE.imh][STATE.imd] is None:
            STATE.imh = 0
            STATE.imd = 0
        highlighting(STATE.imh, STATE.imd)

curses.endwin()
