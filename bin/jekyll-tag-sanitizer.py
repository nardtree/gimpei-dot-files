#!/bin/env python3
import glob
from pathlib import Path
import re
import os
import datetime

for path in Path(".").glob("*"):
    path = path.absolute()
    file_name = path.name
    parent = path.parent
    parent_dir_name = parent.name
    if parent_dir_name == "adhoc-analytics":
        if re.search("\.md$", file_name):
            # print(parent_dir_name, file_name)
            lines = []
            with path.open("r") as fp:
                for line in fp:
                    lines.append(line)
            cnt = 0
            start = None
            end = None
            for idx, line in enumerate(lines):
                if "---" in line:
                    if cnt == 0:
                        start = idx
                        cnt += 1
                        continue
                    if cnt == 1:
                        end = idx
                        cnt += 1
                        break
            # print(start, end, lines[start:end])
            has_config = False
            has_update_dates = False
            for idx, line in enumerate(lines):
                if start < idx < end:
                    if "config" in line:
                        has_config = True
                    if "update_dates" in line:
                        has_update_dates = True

            # has_configがTrueだったら消していいか聞く
            if has_config is True:
                print(file_name)
                ans = input("configフィールドがあるようです。消していいですか？(Y/n)")
                if ans == "Y":
                    parts = [line for idx, line in enumerate(lines) if not("config" in line and (start < idx < end))]
                    with path.open("w") as fp:
                        fp.write("".join(parts))
            if has_config is True:
                continue

            if has_update_dates is False:
                log_dates = []
                for git_line in os.popen(f"git log {file_name}").read().split("\n"):
                    if "Date:" in git_line:
                        if "+0900" in git_line:
                            pt = datetime.datetime.strptime(git_line, "Date:\t%a %b %d %H:%M:%S %Y +0900")
                            log_dates.append(pt.strftime("%Y-%m-%d"))
                        elif "+0000" in git_line:
                            pt = datetime.datetime.strptime(git_line, "Date:\t%a %b %d %H:%M:%S %Y +0000")
                            log_dates.append(pt.strftime("%Y-%m-%d"))
                        else:
                            raise Exception(f"{git_line}")
                if len(log_dates) == 0:
                    continue
                log_dates = sorted(log_dates, reverse=True)
                ans = input(f"{file_name}には update_datesが存在しないようです。追加してもいいですか？")
                if ans == "Y":
                    insert_update_dates = "update_dates: [" + ",".join([f'"{x}"' for x in log_dates]) + "]\n"
                    insert_sort_key = f'sort_key: "{log_dates[0]}"\n'
                    # endの前に入れる
                    lines.insert(end, insert_update_dates)
                    lines.insert(end, insert_sort_key)
                    with path.open("w") as fp:
                        fp.write("".join(lines))

    if parent_dir_name == "configs":
        if re.search("\.md$", file_name):
            lines = []
            with path.open("r") as fp:
                for line in fp:
                    lines.append(line)
            cnt = 0
            start = None
            end = None

            for idx, line in enumerate(lines):
                if "---" in line:
                    if cnt == 0:
                        start = idx
                        cnt += 1
                        continue
                    if cnt == 1:
                        end = idx
                        cnt += 1
                        break
            has_config = False
            has_update_dates = False
            for idx, line in enumerate(lines):
                if start < idx < end:
                    if "config" in line:
                        has_config = True
                    if "update_dates" in line:
                        has_update_dates = True
            if has_config is False:
                print(file_name, "configが含まれていません")
            if has_update_dates is False:
                log_dates = []
                for git_line in os.popen(f"git log {file_name}").read().split("\n"):
                    if "Date:" in git_line:
                        if "+0900" in git_line:
                            pt = datetime.datetime.strptime(git_line, "Date:\t%a %b %d %H:%M:%S %Y +0900")
                            log_dates.append(pt.strftime("%Y-%m-%d"))
                        elif "+0000" in git_line:
                            pt = datetime.datetime.strptime(git_line, "Date:\t%a %b %d %H:%M:%S %Y +0000")
                            log_dates.append(pt.strftime("%Y-%m-%d"))
                        else:
                            raise Exception(f"{git_line}")
                        # print(git_line, pt)
                if len(log_dates) == 0:
                    continue
                log_dates = sorted(log_dates, reverse=True)
                ans = input(f"{file_name}には update_datesが存在しないようです。追加してもいいですか？")
                if ans == "Y":
                    insert_update_dates = "update_dates: [" + ",".join([f'"{x}"' for x in log_dates]) + "]\n"
                    insert_sort_key = f'sort_key: "{log_dates[0]}"\n'
                    # endの前に入れる
                    lines.insert(end, insert_update_dates)
                    lines.insert(end, insert_sort_key)
                    with path.open("w") as fp:
                        fp.write("".join(lines))

