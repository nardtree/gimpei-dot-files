#!/usr/bin/env python3
import time
from subprocess import Popen
from subprocess import PIPE
import asyncio
import sys
import os

buf = b""


async def reader(proc):
    global buf
    fileno = proc.stdout.fileno()
    # 逐次的に処理できた内容から見ていくことができる
    while True:
        bc = os.read(fileno, 2)
        if not bc:
            break
        print("scanned", bc.decode("utf8").strip(), end="\n")
        buf += bc


async def writer(proc):
    global buf
    fileno = proc.stdin.fileno()
    # ここでなにか推論したいデータを入力する
    for ch in "hello world!E":

        os.write(fileno, bytes(ch, "utf8"))
        os.write(fileno, bytes("\n", "utf8"))
        time.sleep(0.1)
        proc.stdin.flush()


async def main():
    with Popen(["python3", "_sample_predictor.py"], stdin=PIPE, stdout=PIPE) as proc:

        task_writer = asyncio.create_task(writer(proc))
        task_reader = asyncio.create_task(reader(proc))

        await task_reader
        await task_writer


asyncio.run(main())
