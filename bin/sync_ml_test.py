#!/usr/bin/env python3
import time
from subprocess import Popen
from subprocess import PIPE
import asyncio
import sys
import os

buf = b""


def reader(proc):
    global buf
    fileno = proc.stdout.fileno()
    while True:
        bc = os.read(fileno, 2)
        if not bc:
            break
        print("scanned", bc.decode("utf8").strip(), end="\n")
        buf += bc


def writer(proc, ch):
    global buf
    fileno = proc.stdin.fileno()
    os.write(fileno, bytes(ch, "utf8"))
    os.write(fileno, bytes("\n", "utf8"))
    time.sleep(0.1)
    proc.stdin.flush()


def main():
    # ここでなにか推論したいデータを入力する
    for ch in "hello world!E":
        with Popen(
            ["python3", "_sample_predictor.py"], stdin=PIPE, stdout=PIPE
        ) as proc:
            writer(proc, ch)
            writer(proc, "E")
            print("input", ch)
            reader(proc)


main()
