
head=`pmset -g batt | head -n 1`
percentage=`pmset -g batt | grep -o '[0-9][0-9]%'` 

if [[ $head =~ "AC Power" ]]; then
  echo "#[fg=black,bold,bg=#00FF9B]  #[default]"
else
  echo "#[fg=black,bold,bg=#ffb900]  $percentage #[default]"
fi

