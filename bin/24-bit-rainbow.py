#!/bin/env python3

import sys
import os
import random


def setBackgroundColor(a, b, c):
    sys.stdout.write(f"\x1b[48;2;{a};{b};{c}m ")


def rainbowColor(arg1):
    h = arg1 // 43
    f = arg1 - 43 * h
    t = f * 255 // 43
    q = 255 - t

    if h == 0:
        return [255, t, 0]
    elif h == 1:
        return [q, 255, 0]
    elif h == 2:
        return [0, 255, t]
    elif h == 3:
        return [0, q, 255]
    elif h == 4:
        return [t, 0, 255]
    elif h == 5:
        return [255, 0, q]
    else:
        print(arg1)
        return [0, 0, 0]


cols, _ = os.get_terminal_size()

random_start = random.randint(0, 256)
for i in range(random_start, random_start + cols):
    setBackgroundColor(*rainbowColor(i % 258))

sys.stdout.write("\x1b[0m\n")
